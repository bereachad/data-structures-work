#include "MysteryCard.h"
#include <fstream>
#include <iostream>
#include <string>

bool MysteryCard:: open_IO(){
    do{
    inFile.clear();
    readFileName="";
    ask_filenames();
    inFile.open(readFileName.c_str());
    }
    while(inFile.fail());

    return true;

}


void MysteryCard:: ask_filenames(){

    cout << "Welcome. Please type in the name of the file to read the numbers.\n";
    cin >> readFileName;
}

bool MysteryCard::read_file(MysteryCard *newDeck){
    char more;
    //MysteryCard* deck;
    deck = new MysteryCard[size];
    //MysteryCard *newDeck;
    MysteryCard temp;

    inFile >> deck[0];

    while (inFile >> temp){

        newDeck = new MysteryCard[size+1];
        int i;
        for (i=0 ;deck[i] < temp && i < size ; i ++ ) {
               newDeck[i]= deck[i];
            }

        //put temp
            newDeck[i] = temp;

        //place all cards after temp into the new deck;
        for ( ; i < size ; i ++ )
        {
            newDeck[i+1] = deck[i];
        }
        size ++ ;
         //delete old deck before reallocation
            delete [] deck;
        deck = newDeck; //new MysteryCard [size];
    }
    cout << endl<<endl;
    cout<< "Here are the cards you gave me sorted!"<<endl<<endl;

    for (int q= 0; q < size ; q++)
        cout <<"deck[ " <<q <<" ] " << deck[q]<<endl;

    do{
    cout <<"Do You have any addition cards you would like to add?(Y/N)";
    cin >> more;

    if (more == 'Y' || more == 'y'){

        cin >> temp;
        newDeck = new MysteryCard[size+1];
        int i;
        for (i=0 ;deck[i] < temp && i < size ; i ++ ) {
               newDeck[i]= deck[i];
            }

        //put temp
            newDeck[i] = temp;

        //place all cards after temp into the new deck;
        for ( ; i < size ; i ++ )
        {
            newDeck[i+1] = deck[i];
        }
        size ++ ;
         //delete old deck before reallocation
            delete [] deck;
        deck = newDeck; //new MysteryCard [size];

        for (int q= 0; q < size ; q++)
            cout <<"deck[ " <<q <<" ] " << deck[q]<<endl;
   }

}while (more == 'Y' || more=='y');

    return true;

}

void MysteryCard::addMore( MysteryCard*newDeck, MysteryCard& temp){
    int userinput;
    MysteryCard *addDeck;

        newDeck = new MysteryCard[size+1];
        int i;
        for (i=0 ;deck[i] < temp && i < size ; i ++ ) {
               newDeck[i]= deck[i];
            }

        //put temp
            newDeck[i] = temp;

        //place all cards after temp into the new deck;
        for ( ; i < size ; i ++ )
        {
            newDeck[i+1] = deck[i];
        }
        size ++ ;
         //delete old deck before reallocation
            delete [] deck;
        deck = newDeck; //new MysteryCard [size];

    cout << endl<<endl;
    cout<< "Here are the cards you gave me sorted!"<<endl<<endl;

    for (int q= 0; q < size ; q++)
        cout <<"deck[ " <<q <<" ] " << deck[q]<<endl;



}

const MysteryCard& MysteryCard:: operator = (const MysteryCard& rhs){
    if (this != &rhs)
    {
        myValue = rhs.myValue;
    }
return* this;
}


istream& operator >> (istream &in, MysteryCard& theCard){
        in >> theCard.myValue;
        return in;
}

// The output function, which is a friend of the class and is not a
// member function of the Card class. Note that because it is a
// general output stream, it can display to both file and the screen.
ostream& operator << ( ostream& out, MysteryCard& theMysteryCard ) {
  out << "MC: " << theMysteryCard.get_value();
  return out;
}

// The comparison operators. Both less and greater than have been
// implemented.
bool operator < ( MysteryCard& preCard, MysteryCard& postCard ) {
  return preCard.get_value() < postCard.myValue;
}

bool operator > ( MysteryCard& preCard, MysteryCard& postCard ) {
  return preCard.get_value() > postCard.myValue;
}

bool operator == ( MysteryCard& preCard, MysteryCard& postCard ) {
  return preCard.get_value() == postCard.myValue;
}
