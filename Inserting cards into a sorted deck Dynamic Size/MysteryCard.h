#include <ostream>
#include <fstream>
#include <string>
using namespace std;


#ifndef MYSTERYCARD_H
#define MYSTERYCARD_H

class MysteryCard{
 public:

  MysteryCard() { myValue = 0; size = 1 ;}

  void change( int newValue ) {
    myValue = newValue;
  }

  bool open_IO();
  void ask_filenames();

  //pre condition: File has been opened with sucess.
  //post condition: file has been read into an array and is sorted correctly
  bool read_file(MysteryCard *newDeck);

  //precondition: File has been sorted, and user says there are more
  //post condition: new card is put in the deck
  void addMore( MysteryCard*newDeck, MysteryCard &temp);

  const MysteryCard& operator = (const MysteryCard&);

  friend istream& operator >> (istream& in, MysteryCard &theCard );
  friend ostream& operator << ( ostream& out, MysteryCard &theCard );
  friend bool operator < ( MysteryCard &preCard, MysteryCard &postCard );
  friend bool operator > ( MysteryCard &preCard, MysteryCard &postCard );
  friend bool operator == ( MysteryCard &preCard, MysteryCard &postCard );
  //void operator = ( int ) {}

 private:
    int size; // size of the array
    MysteryCard* deck; // mysterycard pointer for old deck
  // Private member functions to extract the information from the card
  // class.
  int get_value() const {
    return myValue;
  }

  int myValue;

  // input file names and stream to get the data
  string readFileName;
  ifstream inFile;


};

#endif
