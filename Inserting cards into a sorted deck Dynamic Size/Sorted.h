#include <ostream>
#include <fstream>
#include <string>
#include "MysteryCard.h"

using namespace std;

#ifndef SORTED_H
#define SORTED_H

class SortedPile{

    public:
        SortedPile ();

        void start_sorting();

    private:
        MysteryCard *newDeck;

};

#endif
