/**********************************************************
 * File: BinaryNumber.cc
 **********************************************************/
#include "BinaryNumber.h"

BinaryNumber::BinaryNumber() {
  leastSignificantBit = NULL;
}

Bit*
BinaryNumber::get_least_significant_bit() const {
  return leastSignificantBit;
}

void
BinaryNumber::convert_decimal_to_binary( const int decimal ) {
  if( decimal < 0 ) return;

  /* Now use the algorithm to convert a decimal number to binary. It
     would repeatedly divide the number by 2 and keep the
     remainder. It will build the binary number this way, and due to
     the way that a linked list is created,  */

  // Start the process with the first binary number.
  leastSignificantBit = new Bit;
  if( decimal%2 == 1 ) leastSignificantBit->set_bit();
  else leastSignificantBit->clear_bit();

  // Now loop through the decimal and convert to binary.
  Bit* pntr = leastSignificantBit;
  int remainder = 0;
  int quotient = decimal/2;

  while( quotient > 0 ) {
    remainder = quotient % 2;
    pntr->add_next_bit( remainder == 1 );
    pntr = pntr->get_next_bit(); // Advance the pointer
    quotient = quotient / 2;
  }

}

void
BinaryNumber::remove_all() {

  if( leastSignificantBit == NULL ) return;
  Bit* trailer = leastSignificantBit;
  Bit* forward = trailer->get_next_bit();

  delete trailer;

  while( forward != NULL ) {
    trailer = forward;
    forward = forward->get_next_bit();
    delete trailer;
  }
}

/**********************************************************
 * FUNCTION TO DESIGN AND IMPLEMENT!

 I have decided to look ahead becaue I felt it was more efficent than
 creating another pointer.  The program works well, and even though it uses one more conditional statement, it is still less intensive if caught early than moving pointers around, as well as easier to understand.
 **********************************************************/
void
BinaryNumber::increment() {
    bool A = true;
    Bit* B  = leastSignificantBit;


   while (B->get_next_bit() != NULL){
        if ( A != B->get_bit() ){ // no carry, nothing to do but make b
            B->set_bit();
            A = false;
            break;
        }
        else{
            A = true;
            B ->clear_bit();
            B = B->get_next_bit();
     }
    }

    if ( B->get_next_bit() == NULL){
        //check to see if B is false
        if (B->get_bit() == false){
            B->set_bit();
        }
        else  // create a new Bit
        {
            B->clear_bit();
            B->add_next_bit(1);

        }
    }

}


#include <string>
ostream&
operator << ( ostream& outs, BinaryNumber& theNumber ) {
  Bit* bitPntr = theNumber.leastSignificantBit;
  string output = "";
  while( bitPntr != NULL ) {
    if( bitPntr->get_bit() ) output = "1"+output;
    else output = "0"+output;
    bitPntr = bitPntr->get_next_bit();
  }
  outs << output;
  return outs;
}

