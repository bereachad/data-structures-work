/**********************************************************
 * File: BinaryNumber.h
 * CSC236: A07_BitAdder.html

 **********************************************************/

#include <ostream>
#include <iostream>
// the file Bit.h already includes <cstdlib> and uses namespace std
#include "Bit.h"

class BinaryNumber {
 public:
  BinaryNumber();

  // ACCESSOR FUNCTION
  Bit* get_least_significant_bit() const;

  // precondition: The decimal input >= 0
  // postcondition: leastSignificantBit will point to the first link
  // of a linked list representing the "decimal" number in reverse
  // order (least to most significant bit
  void convert_decimal_to_binary( const int decimal );

  // OVERLOADED OUTPUT STREAM OPERATOR
  friend ostream& operator << ( ostream& outs, BinaryNumber& myBN );

  // precondition: none
  // postcondition: All the links in the linked list started by
  // leastSignificantBit will be de-allocated.
  void remove_all();

  // You are to implement this function that will increment the binary
  // number stored in a linked list by one, making sure to propogate any
  // carries that are generated.

  // For example, if the number 15 is stored as "1111" and this
  // function is called,the result would be "10000" (really
  // represented as 0->0->0->0->1, where the carry "rippled" up the
  // bits, and an additional bit was added at the end because the 4th
  // 1 really became a "10"
  void increment();

 private:
  Bit* leastSignificantBit;
};
