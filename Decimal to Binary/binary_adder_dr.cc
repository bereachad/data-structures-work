/*******************************************************
 * File: binary_adder_dr.cc
 *
 * This file contains the interface with the user to create an object
 * that stores the input binary number as a linked list, them
 * continually calls increment until the user tells it to stop.  It
 * then outputs the results to the user.
 *******************************************************/

// BinaryNumber.h includes "Bit.h" that includes <cstdlib> and 
// uses namespace std
#include "BinaryNumber.h"

#include <iostream>
int 
main() {
  int input;
  BinaryNumber theNumber;
  cout << "input a number to convert to binary! ";
  cin >> input;
  theNumber.convert_decimal_to_binary( input );

  cout << "In binary, the number is " << theNumber << endl;

  char response;
  cout << "Would you like to increment it (y/n)? ";
  cin >> response;
  while( response == 'Y' || response == 'y' ) {
    
    theNumber.increment();
    cout << "Incremented, the number is: " << theNumber << endl;
    cout << "Would you like to increment it (y/n)? ";
    cin >> response;
  }
  theNumber.remove_all();
}

