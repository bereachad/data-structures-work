#include "Dijkstra.h"
#include <iostream>

using namespace std;


dijkstra::dijkstra(){

    // ALOCATION OF DYNAMIC ARRAYS TO THE PROPER SIZE!
    predecessor = new float [dannybhelpedme.get_num_vertex()];
    distance = new float [dannybhelpedme.get_num_vertex()];
    mark = new bool [dannybhelpedme.get_num_vertex()];

    //intialize all variables;
    for (int i = 0; i < dannybhelpedme.get_num_vertex(); i ++){
        mark[i] = false;
        predecessor[i] = -1;
        distance[i] = INFINITY;
    }


    // ask the user for the start and finish
    cout<<"Enter the source vertex\n";
    cin>>source;
    source_as_int = static_cast<int>(source) - 65;

    while((source_as_int<0) && (source_as_int>dannybhelpedme.get_num_vertex()-1)) {
        cout<<"Source vertex should be between 0 and "<<dannybhelpedme.get_num_vertex()-1<<endl;
        cout<<"Enter the source vertex again\n";
        cin>>source;
        source_as_int = static_cast<int>(source) - 65;
}

    cout <<"Enter the Destination: ";
    cin >> destination;

    destination_as_int = static_cast<int>(destination) - 65;

    while((destination_as_int<0) && (destination_as_int>dannybhelpedme.get_num_vertex()-1)) {
        cout<<"Source vertex should be between 0 and "<<dannybhelpedme.get_num_vertex()-1<<endl;
        cout<<"Enter the Destination vertex again\n";
        cin>>destination;
        destination_as_int = static_cast<int>(destination) - 65;
}

    // this line sets the distance to itself as ZERO!!!
    distance [source_as_int] = 0;
}

void dijkstra::algo(){

    int minDistance = INFINITY; // sets teh minimum distance at the moment to infinity
    int closestUnmarkedNode;
    int count = 0;

    while(count < dannybhelpedme.get_num_vertex()) { // loop while the count is less then then number of vertex's

        closestUnmarkedNode = getClosestUnmarkedNode(); // finds the cloest node to the current node that hasn't been visited
        mark[closestUnmarkedNode] = true; // marks that node as visited at this point

        for(int i=0;i<dannybhelpedme.get_num_vertex();i++) { // while there is still places that can be visited
            if((!mark[i]) && (dannybhelpedme.get_matrix_value(closestUnmarkedNode,i ) >0) ) { // if that place hasn't been visted and the value is not itself (0)
                if(distance[i] > distance[closestUnmarkedNode]+dannybhelpedme.get_matrix_value(closestUnmarkedNode,i)) {
                /*the above line compares the distance to this vertex , see's if it is bigger than the closest unmarked node +  the next node that is closest
                if it is, it replaces the distance, if not goes to the top of the loop*/
                    distance[i] = distance[closestUnmarkedNode]+dannybhelpedme.get_matrix_value(closestUnmarkedNode,i);
                    // sets the previous place visited as the closest unmarked node before we mark it, prevents a preemptive marking
                    predecessor[i] = closestUnmarkedNode;
                }
            }
        }
    count++;
    }
}

float dijkstra:: getClosestUnmarkedNode(){
    float minDistance = INFINITY;
    float closestUnmarkedNode;
    // this loop will go and make sure the distance between the node's isn't greater then the current distance, if it is, it makrs tha tdistance and returns
    // that as the node to be visited!
    for(int i=0;i<dannybhelpedme.get_num_vertex();i++) {
        if((!mark[i]) && ( minDistance >= distance[i])) {
            minDistance = distance[i];
            closestUnmarkedNode = i;
        }
    }
    return closestUnmarkedNode;

}

/*void dijkstra:: output(){
        int i = destination_as_int;
        printPath(i, cout);
        cout<<" ->"<<distance[i]<<endl;
}*/

void dijkstra::printPath(int node, ostream& outs){

    char temp = static_cast<char>(node + 65);

    if(node == source_as_int)
        outs<<temp<<"..";
    else if(predecessor[node] == INFINITY)
        outs<<"No path from "<<source<<"to "<<temp<<endl;
    else {
        printPath(predecessor[node], outs);
        outs<<temp<<"..";
    }
}

ostream& operator << ( ostream& outs, dijkstra& me ) {
    for(int i=0;i<me.dannybhelpedme.get_num_vertex();i++) {
        if ( i == me.destination_as_int){
            me.printPath(i, outs);
            outs<<" ->"<<me.distance[i]<<endl;
            }
        }

}
