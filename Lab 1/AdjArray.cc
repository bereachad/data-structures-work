#include "AdjArray.h"
#include <iostream>
#include <cstdlib>


using namespace std;


AdjArray::AdjArray() {

    num_vertex = 0;
    readFileName = "";
    if ( open_IO() == true ){ // if able to open the files, read it in
        read_matrix();
        close_IO();
    }

}

void AdjArray:: read_matrix(){
    float temp; // temp value, used in the looping of input to make sure we aren't at the end.
    int row(0), col(0);

    int counter(0);
    inFile >> num_vertex;

    //create the dynamic array before we fill it up!
	matrix = new float* [num_vertex];
	for (int i = 0; i <num_vertex; i ++ ){
		matrix[i] = new float [num_vertex];
		}

    while ( inFile >> temp){
        if (counter == 5){ // we need to go to the next row !!
            row ++;
            col = 0;
            counter =0;
        }

        matrix[row][col] = temp;
        col++;
        counter ++;
    }



}

bool AdjArray::open_IO(){

    do{
    inFile.clear();
    readFileName="";
    ask_filenames();
    inFile.open(readFileName.c_str());
    }
    while(inFile.fail());

    return true;
}


void AdjArray::close_IO() {
    inFile.close();
}

void AdjArray::ask_filenames() {
  cout << "Welcome. Please type in the name of the file to read the numbers.\n";
  cin >> readFileName;
}


int AdjArray:: get_num_vertex(){
    return num_vertex;
}

float AdjArray:: get_matrix_value(int r, int c){
    return  matrix[r][c];
}
