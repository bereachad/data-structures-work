#include "AdjArray.h"
#include <fstream>


using namespace std;

class dijkstra{

    public:
    //constructor
    dijkstra ();

    // precondition: start location has been entered.
    // postcondition: returns the index location of the node that is closest that hasn't been visited yet.
    float getClosestUnmarkedNode();

    //precondition: Array has been made, start and end have been entered.
    // postcondition: will run the alogrithm and find the shortest path!
    void algo();

    //precondition: algo has been run
    //postcondition: will print the path to the screen.
    //THIS FUNCTION IS NON FUNCTIONAL AT THE MOMENT, Can be implemented later by removing comments
    void output();

    //overloaded operator for the output stream
    friend ostream& operator << ( ostream& out, dijkstra& me );

    private:
        // precondition: algo has been made and user initiated a print
        // postcondition: will print to desired stream
        void printPath(int node, ostream& outs);

        float* predecessor; // array of previous location
        float* distance; // array that holds the shortest distance to that point in the matrix.
        bool* mark; // mark as visited or not
        char source; // where do we start?
        char destination; // where we are going?
        int destination_as_int;
        int source_as_int;
        // creation of the adjaceny matrix object.
        AdjArray dannybhelpedme;

};

