#include <string>
#include <fstream>
#define INFINITY 9999

using namespace std;

class AdjArray {
 public:

  // Constructors:
  // Make sure that both arrays are set to blank.
  AdjArray();
  //precondition:
  //postcondition:  Will open the files and return true when there
    // is sucess.
  bool open_IO();
  //precondition: Files are open and everything is read and done
  // post condition: Will close out the the files.
  void close_IO();

// precondition: file has been read
// postcondition:  will return the number of points in the matrix
  int get_num_vertex();

//precondition: file has been placed in teh array
// postcondition: will return value in the array at that index
  float get_matrix_value(int r, int c);

private:
    // function to read the file into the matrix
    void read_matrix();
    // function to ask file names
    void ask_filenames();

    int num_vertex;  // number of vertex on the map
    float** matrix; // creation of dynamic 2D Array

    string readFileName;
    ifstream inFile;

};

