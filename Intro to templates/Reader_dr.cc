/*
New Changes:
    The reader will pad with 0's if there isn't enough  data.  It will pad with 0's for ints and doubles, will pad with Ace of Spades for a card.
    There is a new display format for the output.

To run the program, the user is prompted with a switch statment and menu.  They select what type of "item" they are creating.  It will then call the template class with that type and read in from a file, than output to a file.



Response to Question:
    The primary advantage to using a template is reusability of code.  You are able to use the same class definintions for different types if you are wanting the same desired output.  For example a template for a linked list creation, you can make a linked list of all different data types.  You can perform  the functions across different data types.  Also, it is more practical to use this rather than global replace, because a replaece won't allow you to use the same class for two different data types in the same prgroam unless you have two different includes.  Also templates allow you to have different templates within the same class for a different type of input using template specialization.




*/


#include "Reader.h"
#include <iostream>

using namespace std;

void menu();

int main(){
    int menu_choice;

    cout<<"*******************************************"<<endl;
    cout<<"******     Assignment 08       ************"<<endl;
    cout<<"*******************************************"<<endl<<endl<<endl<<endl;

    cout<<"What would you like to create?"<<endl;

    menu();
    cin>> menu_choice;

    switch (menu_choice){
        case 1:{
            Reader<int> integers;
            if (integers.open_IO()){
                integers.read_array();
                integers.write_array();
                integers.close_IO();
            }
            break;
        }
        case 2:{
            Reader<double> doubles;
            if (doubles.open_IO()){
                doubles.read_array();
                doubles.write_array();
                doubles.close_IO();
            }
            break;
        }
        case 3:{
            Reader<Card> cards;
            if (cards.open_IO()){
                cards.read_array();
                cards.write_array();
                cards.close_IO();
            }
            break;
        }
        default:
            cout<<"You selected an invalid choice, exiting";
    }

}

void menu(){
    cout <<"1. Integer"<<endl<<"2. Double"<<endl<<"3. Card"<<endl;


}