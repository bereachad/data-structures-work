#include <fstream>
#include <string>
#include <iostream>
#include <stack>

using namespace std;

class Maze{
    public:
        Maze(){
            ask_filenames();
            read_file();
            print_maze();
        }; // constructor

        void ask_filenames();
        //pre condition: none
        //post condition:file names input

        void read_file();
        //pre condition: file name has been input
        //post condition: reads in the maze dementions and maze

        void print_maze();
        //pre condition: maze read in
        //post condition: prints maze to screen

        void print_path();
        //pre condition: stack not empty and path found
        //post condition: path printed to screen

        bool explore();
        //false while exploring maze
        //true when all accessable places in maze are explored

    private:
        char ** maze_layout; // pointer creation for a 2d dynamic array
        int maze_width;      // int for maze width
        int maze_height;     // int for maze height
        int my_x;            // int for x position
        int my_y;            // int for y position
        int start_x;         // int to keep track of x start point
        int start_y;         // int to keep track of y start point

        stack <int> path;    // a stack to store the path we are taking, used whne we need to know how to get to a treasure.

        string fileName;
        ifstream inFile;

};
