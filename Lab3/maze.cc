#include "maze.h"

void Maze::ask_filenames(){
    cout << "What is the file name?";
    cin>> fileName;
}

void Maze::read_file(){
    //open the infile.
    inFile.open(fileName.c_str());
    inFile >> maze_height;
    inFile >> maze_width;

    //dynamically create the 2D array!
    maze_layout = new char *[maze_height];

    for (int i= 0; i < maze_height; i++){
        maze_layout[i]=new char[maze_width];
    }

    //fill the ARRAY!!!
   for (int i=0; i < maze_height; i ++){
        for (int c = 0; c < maze_width; c++){
            inFile >>maze_layout[i][c];
            if (maze_layout[i][c] == 'M'){
                my_y=i;
                my_x =c;
                start_x = c;
                start_y= i;
            }
        }
    }
}

void Maze::print_maze(){
    for (int i=0; i < maze_height; i ++){
            for (int c = 0; c < maze_width; c++)
                cout<<maze_layout[i][c]<<" ";
            cout<<endl;
            }
}

bool Maze:: explore(){
    char temp;
    int direction; // used to read what was on the stack, and interpret it.

    // due to the fact we are certain that there is a maze wall, the index of me will never be less than (1,1) , so we can't go out of hte array in this condition.
   while ( maze_layout[my_y + 1][my_x] != '*' || maze_layout[my_y - 1][my_x] !='*' || maze_layout[my_y ][my_x + 1] != '*' || maze_layout[my_y ][my_x - 1] != '*'){

        // check Up
        if( maze_layout[my_y - 1] [my_x] !='W' && maze_layout[my_y-1] [my_x] != '*') {
            if( maze_layout[my_y - 1] [my_x] == 'T'){
                print_path();
            }
            maze_layout[my_y] [my_x] = '*';
            maze_layout[my_y - 1] [my_x] = 'M';
            my_y= my_y-1;
            path.push(1);
        }

        // check Right
        else if( maze_layout[my_y] [my_x + 1] != 'W' && maze_layout[my_y] [my_x + 1] != '*') {
            if ( maze_layout[my_y] [my_x + 1] == 'T'){
                print_path();
            }
            maze_layout[my_y] [my_x] = '*';
            maze_layout[my_y] [my_x + 1] = 'M';
            my_x = my_x+1;
            path.push(2);
        }

        // check down
        else if( maze_layout[my_y + 1 ] [my_x] != 'W' && maze_layout[my_y +1] [my_x] != '*') {
            if ( maze_layout[my_y + 1] [my_x] == 'T'){
                print_path();
            }
            maze_layout[my_y] [my_x] = '*';
            maze_layout[my_y + 1] [my_x] = 'M';
            my_y = my_y+1;
            path.push(-1);
        }

        // check left
        else if( maze_layout[my_y] [my_x - 1] != 'W' && maze_layout[my_y] [my_x - 1] != '*') {
            if ( maze_layout[my_y] [my_x - 1] == 'T'){
                print_path();
            }
            maze_layout[my_y] [my_x] = '*';
            maze_layout[my_y] [my_x - 1] = 'M';
            my_x = my_x-1;
            path.push(-2);
        }
        else  // we have no where to go new, because we are blocked by walls, move back a step according to the stack;
        {


             // set up a loop to keep poping off stack untill a valid space is found;
            do{

            if (path.empty()){ // the maze is completed!!
                return true;
            }

            direction =path.top();

            // CHECK TO SEE IF STACK IS EMPTY, if it is, maze is done.

            if ( direction == 1) // we need to go down
            {
                maze_layout[my_y][my_x] = '*';
                maze_layout[my_y + 1 ][my_x]='M';
                my_y = my_y+1;
                print_maze();
                cin >>temp;
            }
            else if ( direction == -1) // we need to go up
            {
                maze_layout[my_y][my_x] = '*';
                maze_layout[my_y - 1 ][my_x]='M';
                my_y = my_y-1;
                print_maze();
                cin >>temp;
            }
            else if ( direction == 2) // we need to go left
            {
                 maze_layout[my_y][my_x] = '*';
                maze_layout[my_y ][my_x-1]='M';
                my_x = my_x-1;
                print_maze();
                cin >>temp;
            }
            else if ( direction == -2) // we need to go right
            {
                maze_layout[my_y][my_x] = '*';
                maze_layout[my_y ][my_x+1]='M';
                my_x = my_x+1;
                print_maze();
                cin >>temp;
            }

                path.pop();

            }while (maze_layout[my_y + 1][my_x] != '.' && maze_layout[my_y - 1][my_x] !='.' && maze_layout[my_y ][my_x + 1] != '.' && maze_layout[my_y ][my_x - 1] != '.');
        }

    print_maze();
    cin>> temp;

    }// this loop breaks when all surrounding spots are * or W

}


void Maze::print_path(){
    int size = path.size(); // int that is the size of the path to make an array
    int path_ar[size];      // array of int to but the stack in to print out the right way
    int i = size;           // index for the array

    while (!path.empty())   // copy all values of the stack into the array,
      {
         path_ar[i] = path.top();
         path.pop();
         i -- ;
      }

      cout <<"************* PATH FOUND !!!! ************** " <<endl;

      for (int l= 1; l <=  size; l ++ ) { // iterate through array to print
            if ( path_ar[l] == -1 )
                cout << "SOUTH ->";
            else if (path_ar[l] == 1)
                cout << "NORTH -> ";
            else if (path_ar[l] == -2)
                cout << " WEST -> ";
            else // just an else because its one less calculation
                cout<< " EAST -> ";

      // put the values back into the stack!
      path.push(path_ar[l]);
      }
      cout <<endl;
}
