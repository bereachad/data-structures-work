//deck.cc file

#include "Deck.h"
#include "card.h"
#include <ostream>
#include <string>
#include <cstdlib>

using namespace std;

Deck::Deck() {
    int whichCard = 0;
    for( int suit=0; suit<4; suit++ ) {
      for( int rank=0; rank<13; rank++ ) {
	myCards[whichCard++].change( suit, rank );
      }
    }
  }

void Deck::shuffle() {
    int place = random( ) % SIZE;
    for( int i=0; i<SIZE; i++ ) {
      Card temp = myCards[i];
      myCards[i] = myCards[place];
      myCards[place] = temp;
      place = random( ) % SIZE;
    }
  }

void Deck::sort() {
    for( int i=0; i<SIZE; i++ ) {
      for( int j=i+1; j<SIZE; j++ ) {
	if( myCards[j] < myCards[i] ) {
	  Card temp = myCards[i];
	  myCards[i] = myCards[j];
	  myCards[j] = temp;
	}
      }
    }
  }

// friend function to output the deck, which is NOT a member function
// of the class.
ostream& operator << ( ostream& out, Deck theDeck ) {

  out << "The deck: \n";

  int whichCard = 0;
  for( int j=0; j<13; j++ ) {
    for( int i=0; i<4; i++ )
      out << theDeck.myCards[whichCard++] << "\t";
    out << endl;
  }
  return out;
}



