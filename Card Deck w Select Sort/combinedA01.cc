#include <ostream>
#include <string>
using namespace std;

class Card{
 public:
  enum SUIT {clubs, diams, hears, spads };
  enum RANK {ace=1, two, thr, fou, fiv, six, sev, eig, nin, ten, jac, que, kin};

  Card() { suit = spads; rank = ace; }

  Card( int newSuit, int newRank ) {
    change( newSuit, newRank );
  }

  void change( int newSuit, int newRank ) {
    change_suit( newSuit );
    change_rank( newRank );
  }

  friend ostream& operator << ( ostream& out, Card theCard );
  friend bool operator < ( Card preCard, Card postCard );
  friend bool operator > ( Card preCard, Card postCard );
  void operator = ( int ) {}

 private:

  // private member functions to set the private variables with their
  // corresponding values: integer input -> enumerated type;
  void change_rank( int newRank ) {
    if( newRank == ace ) rank = ace;
    else if( newRank == two ) rank = two;
    else if( newRank == thr ) rank = thr;
    else if( newRank == fou ) rank = fou;
    else if( newRank == fiv ) rank = fiv;
    else if( newRank == six ) rank = six;
    else if( newRank == sev ) rank = sev;
    else if( newRank == eig ) rank = eig;
    else if( newRank == nin ) rank = nin;
    else if( newRank == ten ) rank = ten;
    else if( newRank == jac ) rank = jac;
    else if( newRank == que ) rank = que;
    else if( newRank == kin ) rank = kin;
  }

  void change_suit( int newSuit ) {
    if( newSuit == clubs ) suit = clubs;
    else if( newSuit == spads ) suit = spads;
    else if( newSuit == diams ) suit = diams;
    else if( newSuit == hears ) suit = hears;
  }

  // Private member functions to extract the information from the card
  // class.
  string get_rank() const {
    if( rank == ace ) return "ace";
    if( rank == two ) return "two";
    if( rank == thr ) return "three";
    if( rank == fou ) return "four";
    if( rank == fiv ) return "five";
    if( rank == six ) return "six";
    if( rank == sev ) return "seven";
    if( rank == eig ) return "eight";
    if( rank == nin ) return "nine";
    if( rank == ten ) return "ten";
    if( rank == jac ) return "jack";
    if( rank == que ) return "queen";
    if( rank == kin ) return "king";
    return "get_rank: error";
  }

  string get_suit() const {
    if( suit == diams ) return "D";
    if( suit == hears ) return "H";
    if( suit == spads ) return "S";
    if( suit == clubs ) return "C";
    return "get_suit: error";
  }

  SUIT suit;
  RANK rank;
};

// The output function, which is a friend of the class and is not a
// member function of the Card class. Note that because it is a
// general output stream, it can display to both file and the screen.
ostream& operator << ( ostream& out, Card theCard ) {
  out << theCard.get_rank() << " of " << theCard.get_suit();
  return out;
}

// The comparison operators. Both less and greater than have been
// implemented.
bool operator < ( Card preCard, Card postCard ) {
  int preVal = (preCard.suit*100)+preCard.rank;
  int postVal = (postCard.suit*100)+postCard.rank;
  return preVal < postVal;
}

bool operator > ( Card preCard, Card postCard ) {
  int preVal = (preCard.suit*100)+preCard.rank;
  int postVal = (postCard.suit*100)+postCard.rank;
  return preVal > postVal;
}

// for the random number generator
#include <cstdlib>
using namespace std;
class Deck {

  static const int SIZE = 52;

 public:
  // Constructor: Create a new deck of sorted cards
  Deck() {
    int whichCard = 0;
    for( int suit=0; suit<4; suit++ ) {
      for( int rank=0; rank<13; rank++ ) {
	myCards[whichCard++].change( suit, rank );
      }
    }
  }

  // Simple suffling function that takes random places in the deck and
  // swaps cards out.
  void shuffle() {
    int place = random( ) % SIZE;
    for( int i=0; i<SIZE; i++ ) {
      Card temp = myCards[i];
      myCards[i] = myCards[place];
      myCards[place] = temp;
      place = random( ) % SIZE;
    }
  }

  // Simple sorting function that uses the selection sort algorithm,
  // where all cards from myCards[0] to myCards[i] is sorted
  void sort() {
    for( int i=0; i<SIZE; i++ ) {
      for( int j=i+1; j<SIZE; j++ ) {
	if( myCards[j] < myCards[i] ) {
	  Card temp = myCards[i];
	  myCards[i] = myCards[j];
	  myCards[j] = temp;
	}
      }
    }
  }

  friend ostream& operator << (ostream& out, Deck theDeck );

 private:
  Card myCards[SIZE];
};

// friend function to output the deck, which is NOT a member function
// of the class.
ostream& operator << ( ostream& out, Deck theDeck ) {

  out << "The deck: \n";

  int whichCard = 0;
  for( int j=0; j<13; j++ ) {
    for( int i=0; i<4; i++ ) 
      out << theDeck.myCards[whichCard++] << "\t";
    out << endl;
  }
  return out;
}

#include <iostream>
int main() {
  Deck myDeck;
  myDeck.shuffle();
  cout << myDeck;
  myDeck.sort();
  cout << myDeck;
}
