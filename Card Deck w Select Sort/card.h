//card.h file

#ifndef CARD_H
#define CARD_H
#include <ostream>
#include <string>
using namespace std;

class Card{
 public:
  enum SUIT {clubs, diams, hears, spads };
  enum RANK {ace=1, two, thr, fou, fiv, six, sev, eig, nin, ten, jac, que, kin};

  Card() { suit = spads; rank = ace; };

  Card( int newSuit, int newRank );

  void change( int newSuit, int newRank );

  friend ostream& operator << ( ostream& out, Card theCard );
  friend bool operator < ( Card preCard, Card postCard );
  friend bool operator > ( Card preCard, Card postCard );
  void operator = ( int ) {}

 private:

  // private member functions to set the private variables with their
  // corresponding values: integer input -> enumerated type;
  void change_rank( int newRank );

  void change_suit( int newSuit );

  // Private member functions to extract the information from the card
  // class.
  string get_rank() const ;

  string get_suit() const ;

  SUIT suit;
  RANK rank;
};

#endif
