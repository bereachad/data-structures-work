//deck.h file
#ifndef DECK_H
#define DECK_H

#include "card.h"
#include <ostream>
#include <string>
#include <cstdlib>

using namespace std;

class Deck {

  static const int SIZE = 52;

 public:
  // Constructor: Create a new deck of sorted cards
  Deck();

  // Simple suffling function that takes random places in the deck and
  // swaps cards out.
  void shuffle();
  // Simple sorting function that uses the selection sort algorithm,
  // where all cards from myCards[0] to myCards[i] is sorted
  void sort();

  friend ostream& operator << (ostream& out, Deck theDeck );

 private:
  Card myCards[SIZE];
};

// friend function to output the deck, which is NOT a member function
// of the class.
ostream& operator << ( ostream& out, Deck theDeck );

#endif
