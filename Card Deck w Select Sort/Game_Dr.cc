//Driver file

#include <iostream>
#include <string>
#include <cstdlib>
#include "Deck.h"
#include "card.h"

int main() {
  Deck myDeck;
  myDeck.shuffle();
  cout << myDeck;
  myDeck.sort();
  cout << myDeck;
}

