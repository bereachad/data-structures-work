//card cc file

#include <ostream>
#include <string>
#include "card.h"

using namespace std;

Card::Card( int newSuit, int newRank ) {
    change( newSuit, newRank );
  }

string Card:: get_suit() const {
    if( suit == diams ) return "D";
    if( suit == hears ) return "H";
    if( suit == spads ) return "S";
    if( suit == clubs ) return "C";
    return "get_suit: error";
  }

string Card::get_rank() const {
    if( rank == ace ) return "ace";
    if( rank == two ) return "two";
    if( rank == thr ) return "three";
    if( rank == fou ) return "four";
    if( rank == fiv ) return "five";
    if( rank == six ) return "six";
    if( rank == sev ) return "seven";
    if( rank == eig ) return "eight";
    if( rank == nin ) return "nine";
    if( rank == ten ) return "ten";
    if( rank == jac ) return "jack";
    if( rank == que ) return "queen";
    if( rank == kin ) return "king";
    return "get_rank: error";
  }

void Card:: change_suit( int newSuit ) {
    if( newSuit == clubs ) suit = clubs;
    else if( newSuit == spads ) suit = spads;
    else if( newSuit == diams ) suit = diams;
    else if( newSuit == hears ) suit = hears;
  }

void Card:: change_rank( int newRank ) {
    if( newRank == ace ) rank = ace;
    else if( newRank == two ) rank = two;
    else if( newRank == thr ) rank = thr;
    else if( newRank == fou ) rank = fou;
    else if( newRank == fiv ) rank = fiv;
    else if( newRank == six ) rank = six;
    else if( newRank == sev ) rank = sev;
    else if( newRank == eig ) rank = eig;
    else if( newRank == nin ) rank = nin;
    else if( newRank == ten ) rank = ten;
    else if( newRank == jac ) rank = jac;
    else if( newRank == que ) rank = que;
    else if( newRank == kin ) rank = kin;
  }

void Card::change( int newSuit, int newRank ) {
    change_suit( newSuit );
    change_rank( newRank );
  }

  // The output function, which is a friend of the class and is not a
// member function of the Card class. Note that because it is a
// general output stream, it can display to both file and the screen.
ostream& operator << ( ostream& out, Card theCard ) {
  out << theCard.get_rank() << " of " << theCard.get_suit();
  return out;
}

// The comparison operators. Both less and greater than have been
// implemented.
bool operator < ( Card preCard, Card postCard ) {
  int preVal = (preCard.suit*100)+preCard.rank;
  int postVal = (postCard.suit*100)+postCard.rank;
  return preVal < postVal;
}

bool operator > ( Card preCard, Card postCard ) {
  int preVal = (preCard.suit*100)+preCard.rank;
  int postVal = (postCard.suit*100)+postCard.rank;
  return preVal > postVal;
}
