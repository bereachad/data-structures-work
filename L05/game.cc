#include "game.h"


game::game(){
    inFile.open("game.in"); // open the input file;
    outFile.open("game.out"); // open of the outFile / creation.
    done = false; // set of boollean to false, we are not done.
    type_of_node = ' '; // blanking of char value

    populate_tree(ROOT); // call to the function to create the intial tree!

    // now we set current to root so we can traverse while playing the game.
    current = ROOT;
}

void game::populate_tree(Node* & place){
    // make sure not EOF;
    if (!inFile.eof()){
        // read in the first character to see if it is a question or guess.
        inFile.get(type_of_node);
        // get the rest of that line.
        getline(inFile, junk);
       // cout <<"Type"<<type_of_node<<endl;
        place = new Node;

        if (type_of_node == 'q' || type_of_node =='Q'){ // it is a question;
           // cout << "Insert Question Node" <<endl;
            getline(inFile,information);
            place->info = information;
            place->type = 'q';

            // remove trash of yes / no lines;
            getline(inFile,junk);
            getline(inFile,junk);
            getline(inFile,junk);// remove junk blank line;

            // populate left
            populate_tree(place->left);
            // we are done going left, go right.
            populate_tree(place->right);
        }
        else if (type_of_node =='g' || type_of_node=='G'){
            getline(inFile,information);
            place->info = information;
            place->type = 'g';
            place->left = NULL;
            place->right = NULL;
            // remove trash blank;
            getline(inFile,junk);
        }
    }
}

void game::play(){
    char temp;

    while(done == false){ // play untill we guess.
        // get node type so we know what to do, either ask yes/no
        // for a question, or ask if correct guess.
        temp = return_node_type();

        if (temp == 'q'){ // we are at a question, prompt user for yes or no.
            cout <<"Question: " <<current->info<<"? Y / N"<<endl;
            cin >>temp;

            if (temp == 'y' || temp =='Y'){ // they answered yes, go left.
                current = current->left;
            }
            else if (temp == 'n' || temp =='N'){ // go to the right
                current = current->right;
            }
        }
        else{
        //else it is a guess.
            cout << "Are you thinking of a " << current->info<<"? Y/N"<<endl;
            cin >> temp;
            if (temp == 'y' || temp =='Y'){ // they answered yes, go left.
                cout << endl<<"**** Yes! I can read your mind!!  *****"<<endl;

                cout << "Would you Like to play again? (Y/N)"<<endl;
                cin >> temp;
                if (temp =='N' || temp == 'n'){
                    // exit the game.
                    cout << "******************************************"<<endl;
                    cout << endl<<"\tThank You for Playing !" <<endl;
                    cout << "******************************************"<<endl;
                    // print to file.
                    current = ROOT;
                    print_out(current);

                    done =true;
                }
                else{ // they want to play again.
                    current = ROOT;
                }
            }
            else if ( (temp == 'n' || temp =='N') && (current->right !=NULL)){
                // we go to the right if the answer is No and there are still nodes to traverse.
                current = current->right;
            }
            else{ // we have to edit the tree.
                add_nodes();
                cout <<endl<< "****************************************************************"<<endl;
                cout << " We have edited the game.  Thank you for your valuable Feedback."<<endl;
                cout << "****************************************************************"<<endl<<endl;
                cout << "Would you Like to play again? (Y/N)"<<endl;
                cin >> temp;
                if (temp =='N' || temp == 'n'){
                    // exit the game.
                    cout <<endl<< "******************************************"<<endl;
                    cout <<endl<< "\tThank You for Playing !" <<endl;
                    cout << "******************************************"<<endl;
                    // put the current node to the root for the purpose of printing;
                    current = ROOT;
                    // print to file.
                    print_out(current);

                    done =true;
                }
                else{ // they want to play again.
                    current = ROOT;
                }
            }
        }
    }
}


void game::add_nodes(){
    // before we add nodes, we have to get the old guess and save that;
    string old_response = current->info;
    char ty; // chars to interact w/ user.
    string in; // data for the question or animal
    string temp;


    // they want to insert a guess node;
    cout << "What is the name of the animal?";
    cin >> in;
    current->left = new Node;
    current->right = new Node;
    current->left->info= in;
    current->left->type = 'g';
    current->right->info = old_response;
    current->right->type ='g';
    // NULL out the new pointers.
    current->right->left = NULL;
    current->right->right = NULL;
    current->left->right = NULL;
    current->left->left = NULL;

    // they want to insert a question node;
    cout<< "What question should I ask for your guess to be true?"<<endl<<"Q:";
    cin >> in;
    getline(cin,temp);
    in+=temp;
    cout << endl<<endl;
    current->info = in;
    current->type = 'q'; // change node type to a question.
}


char game::return_node_type(){
    return current->type;

}

void game:: print_out(Node* place){
    char temp;
    // see what type of node we are printing out to the file.
    temp = return_node_type();
    if (temp =='q'){
        // we are on a question node, we know we have to add in lines
        // for the junk we removed earlier, the yes no and the blank;
        outFile <<"Question:"<<endl;
        outFile << place->info<<endl;
        outFile<<"yes"<<endl;
        outFile <<"no"<<endl;
        outFile<<endl;
        current = place->left;
        print_out(place->left);
        current = place->right;
        print_out(place->right);
    }
    else if (temp == 'g'){ // it was a guess node;
        outFile <<"Guess: "<<endl;
        outFile << place->info<<endl;
        outFile<<endl;
    }


}
