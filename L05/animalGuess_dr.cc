#include "game.h"
#include<iostream>

using namespace std;

int main(){
    cout << "**********************************************"<<endl;
    cout << "****           Animal Guessing Game       ****"<<endl;
    cout << "****             By: Chad Peruggia        ****"<<endl;
    cout << "**********************************************"<<endl;
    cout <<endl<<endl;
    game newOne;
    newOne.play();

}
