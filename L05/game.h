#include <fstream>
#include <iostream>
#include <string>

using namespace std;


struct Node{
    string info;
    Node* left;
    Node* right;
    char type; // type of node, used when playing.
};

class game{
    public:
        // default constructor
        game();

        void populate_tree(Node* &);

        void play();

        void print_out(Node* place);

        void add_nodes();

        char return_node_type();

    private:
        ifstream inFile;
        ofstream outFile;
        string information;
        string response;
        string junk;
        bool done;
        char type_of_node;
        Node* ROOT;
        Node* current;



};
