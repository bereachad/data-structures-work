/*********************************************************************
 Filename: StackDr.cc

 This driver function introduces the user to the purpose of the
 program and then asks the user to input a string with parentheses in
 them. It will output a message announcing a match if the open and
 close parentheses match, and failure otherwise.

 You are to enhance this function so that it matches parentheses (),
 brackets [] and curly braces {}. In other words, the following
 expressions are wellformed:

 1) {[(((())))]}
 2) [{[(()[])]((()))}]

 The following are not well-formed:

 1) {[()))
 2) {{{[
 3) {((]

*********************************************************************/
#include <iostream>
#include "stack_template.h"

using namespace std;

int main() {
  Stack <char> stack;
  char symbol;

  cout << "This program matches parentheses ('s and )'s .\n";
  cout << "Enter an expression that uses parentheses and press return.\n";

  bool balanced = true;

  // repeat analysis until either the string ends with newline or
  // we determine that the parentheses are not matched.
  while (symbol != '\n' && balanced) {
    cin.get(symbol);

    switch (symbol){

    case '(':
      stack.push( symbol );
      break;

    case '{':
      stack.push( symbol );
      break;

    case '[':
      stack.push( symbol );
      break;

    case ']':
        if (stack.top() !='['){// if the top of the stack is not an open for this close.
            balanced = false;
        }
        else // pop it off
            stack.pop();

        break;

    case '}':
        if (stack.top()!='{'){ // if the top of the stack is not an open for this close.
            balanced = false;
        }
        else // pop it off
            stack.pop();

        break;

    case ')':
        if (stack.top() !='('){ // if the top of the stack is not an open for this close.
            balanced = false;
        }
        else // pop it off
            stack.pop();

        break;

    case '\n': // this case means we are at teh new line, determine parenhesis
      if( !stack.empty() ) {
            balanced = false;
      }
      break;
    }
  }


  if( balanced )
    cout << "Expression is well formed." << endl;
  else
    cout << "Expression is not well formed."  << endl;

}
