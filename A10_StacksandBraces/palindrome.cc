#include <iostream>
#include <stack>

using namespace std;

int main() {

  stack<char> myStack;
  char response[50];
  int nChars = 0;

  for( int i=0; i<50; i++ ) response[i] = '\0';

  cout << "Gimme word! ";
  cin >> response; 

  int i=0;
  // go through the response and push everything onto the stack.
  do {
    myStack.push( response[i] );
    i++;
  } while( i<50 && response[i]!= '\0' );

  // check original word with contents of the stack.
  i=0;
  char stackElement;
  do {
    stackElement=myStack.top();
    if( stackElement != response[i] ) {
      cout << "No palindrome!\n";
      return 0;
    }
    myStack.pop();
    i++;
  } while( i<50 && response[i]!= '\0' && !myStack.empty());
  //  we need to check for all these conditions because if any are wrong, then 
  // it is bad!! Bad!! Horrible terrible
  cout << "Palindrome! WOOT!\n";

}
