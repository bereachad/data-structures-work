#include "triple.h"
#include <iostream>
#include <sstream>
#include <assert.h>

using namespace std;

//default constructor
triple:: triple(){
    myValue = 0;
    nextTriple= NULL;
}

triple:: triple (int value){
    set_value(value); // call function to set value, this will make sure positive value is used.
    nextTriple = NULL;
}

triple:: triple(int value, triple* nextOne){
    set_value(value);
    nextTriple = nextOne;
}

//deconstructor
triple:: ~triple(){

}

triple* triple::get_triple(){
    return nextTriple;
}

int triple:: get_value(){
    return myValue;
}


void triple::set_value(const int value){

    if (value < 0 ) {
        cerr << "Incorrect value, setting to 0"<<endl;
        myValue = 0;
    }
    else{
        myValue = value;
    }
}

bool triple::add_value(const int add){
    myValue =+ add;
    if (myValue > 999){
        myValue = myValue - 1000; // subtract 1000 to make it a trip again
        return true;
    }
    else{
        return false;  // there is no carry over
    }
}


//this function would be called as follows: current.insert_triple(newOne);  where newOne is placed after current, and what current was pointing to is now pointed to by newOne;
void triple:: insert_triple( triple* newOne ){

    triple* temp; // create a temp triple pointer.

    assert(nextTriple != NULL);
    temp = nextTriple;
    nextTriple = newOne;
    newOne = temp;
    delete temp;
}

void triple:: insert_triple( const int value ){
    triple* newOne = new triple (value);
    insert_triple(newOne);
}

void triple:: delete_triple(triple* toDelete){
    triple* temp; // pointer to just before the current node;

    if (nextTriple == NULL){ // there is no next element to be deleted.
        cerr <<"There is no next element! You are at the end of the lsit!"<<endl;
    }
    else{
        temp= nextTriple; // set a temp to the next triple;
        if (temp->nextTriple!= NULL)
            {
                this ->set_value(temp->get_value());
                nextTriple = NULL;
                delete temp;
            }
            else{
        // set the nextTriple to where the deleted item was pointing;
        this ->set_value(temp->get_value());
        nextTriple  = temp->nextTriple;
        // delete the temp item now
        delete toDelete;
            }
    }
}


void triple :: delete_triple (const int value){
    triple* temp;
    temp = nextTriple; // set a pointer to that next triple;

    if (nextTriple->myValue == value ) // values are the same! delete it
    {
        delete_triple(temp);
    }

}

string triple:: toString(){

    string values;
    stringstream out;

    if (nextTriple != NULL ) // it is not the last string !
    {

        if (myValue < 100 ) // need one leading 0
            out << "0";
        if (myValue <10) // need another leading 0
            out << out << "0";

        out <<myValue;

        values = out.str();
        return values;
    }
    else {
        out<<myValue;
        values = out.str();
        return values;
    }
}

ostream& operator << (ostream& outs, triple* printTriple){
    triple temp = *printTriple;
    string myvalues = temp.toString();
    outs << myvalues;
    return outs;

}