/*********************************************************************
 Filename: triple.h

 Header information for the triple class used by the BigInt
 class. Each triple for a BigInt is a node in a linked list that will
 store the triple digits between commas in a large integer.

 The functions to implement here are:

 * any necessary constructors; note that the destructor is EMPTY

 * triple* get_triple( )
   returns the pointer to the next triple that this current one
   references, which can be NULL if this current one is at the end.

 * int get_value( )
   returns whatever is in the data region.

 * void set_value( const int )
   sets the value in its data region to hold the value of the input,
   as long as it is nonnegative. If value is negative, it will output
   a warning and set myValue to be 0 instead.  NOTE the constructors
   can use this function!

 * bool add_value( const int )
   the value of input is added to myValue and returns true if there is
   a carry and false otherwise. Make sure that the resulting triple
   stays a triple even when there is a carry!  For example, if value
   was 940 and 500 was added, the function would return true and have
   the value 440.  You may assume that the addend is positive and less
   than 1000 (i.e. it is at most 999)

 * Overloaded functions
     void insert_triple( triple* )
     void insert_triple( const int )
   inserts a triple after the current one, making sure that no
   pointers are lost. The second function will in fact CREATE a new
   triple to store the integer argument value.  NOTE the constructors
   can use this function!

 * Overloaded functions
     void delete_triple( triple* )
     void delete_triple( const int )
   deletes the triple after the current one, making sure that no
   pointers are lost. The second function will only delete the next
   triple if that triple's data is the same as the input parameter.
   NOTE the constructors can use this function!

 * string toString( )
   returns the string representation of the triple, with leading zeros
   if it is not the last triple in the linked list (you can easily
   check this condition!)

 * friend ostream& operator << ( ostream&, triple* )
   outputs the string representation of the triple onto the output
   stream of the program's choice.

*********************************************************************/

#ifndef TRIPLE
#define TRIPLE

#include <ostream>
#include <string>
using namespace std;

class triple {
public:
  /*-----------------------------------------------
    Constructors and blank destructor
    -----------------------------------------------*/
  // default: myValue = 0, and not pointing to anything.
  triple();

  // set myValue=value and current triple is at end of list
  triple( int value );

  // set myValue=value, nextTriple to nextOne
  triple( int value, triple* nextOne );
  ~triple();

  // Accessor functions
  triple* get_triple();
  int get_value();

  // Mutator functions
  void set_value( const int );
  bool add_value( const int );

  void insert_triple( triple* );
  void insert_triple( const int );

  void delete_triple( triple* );
  void delete_triple( const int );

  // Functions to output the triple
  string toString( );
  friend ostream& operator << ( ostream&, triple* );

private:
  triple* nextTriple;
  int myValue;
};

#endif
