#include "triple.h"
#include <iostream>

using namespace std;

int main(){
    int start_value;


    cout <<"Welcome to the Linked List Demonstration!";
    cout <<endl<<endl<<endl;
    cout <<"What is the value in link 1?";
    cin >> start_value;
    cout << " We will also populate 3 more links, each twice as much as the previous value"<<endl;


    triple *head = new triple(start_value); // create the first link
    start_value= start_value*2;

    triple *second = new triple (start_value); // create the second link
    head = new triple (start_value /2 , second); // remake head to point to second;

    start_value= start_value*2;
    triple *third = new triple(start_value); // teh third link!
    second = new triple (start_value / 2, third);


    cout <<"Here are the links !!"<<endl;
    cout <<"Link 1: " << head<<endl;
    cout <<"Link 2: " << second<<endl;
    cout <<"Link 3: " << third<<endl;


}