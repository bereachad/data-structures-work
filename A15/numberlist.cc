#include "numberlist.h"
#include <iostream>
using namespace std;

NumberList::NumberList(){
    int temp;
    inFile.open("numbers.in");
    comp = 0;
    swaps = 0;

    while(inFile >> temp){
        data.push_back(temp);
    }

}

void NumberList::selection_sort(){
    // we start at index of 0, then find the smallest element from here
    // till the end. Swap it to the front, repeat it till at the end.


    for (int nStartIndex = 0; nStartIndex < data.size(); nStartIndex++)
    {
        // nSmallestIndex is the index of the smallest element
        // we've encountered so far.
        int nSmallestIndex = nStartIndex;       // Search through every element starting at nStartIndex+1
        for (int nCurrentIndex = nStartIndex + 1; nCurrentIndex < data.size(); nCurrentIndex++)
        {
            // If the current element is smaller than our previously found smallest
            comp++;
            if (data[nCurrentIndex] < data[nSmallestIndex])
            // Store the index in nSmallestIndex
            nSmallestIndex = nCurrentIndex;
        }
        // Swap our start element with our smallest element
        swaps++;
        swap(data[nStartIndex], data[nSmallestIndex]);
    }
}

vector<int> NumberList::merge_sort(const vector<int>& list){
    char temp;
    vector<int>left;
    vector<int>right;
    vector<int>result;

    if (list.size() <= 1){
        return list; // returns the list if it is only one number.
    }

    int middle = (list.size()/2); // create a pivot point.

    for (int i = 0; i <middle; i ++ ){
        //place all numbers below mid into a vector
        left.push_back(list[i]);
    }

    for (int i = middle; i < list.size(); i ++ ){
        right.push_back(list[i]);
    }

    left = merge_sort(left);
    right = merge_sort(right);

    // the merge takes place after swaps + comparisons.
    result = merge(left,right);

    return result;
}

vector<int> NumberList::merge(const vector<int>& left, const vector<int>&right){
    vector<int>result; // the answer


    int left_size= left.size();
    int right_size = right.size();

    int l_index =0, r_index = 0;

    while (l_index < left_size && r_index < right_size){
        comp++; // comparison took place !
        if (left[l_index] < right[r_index]){
            swaps++;
            result.push_back(left[l_index]);
            l_index ++;
        }
        else{
            result.push_back(right[r_index]);
            r_index ++;
        }
    }

    if (l_index == left_size){
        while(r_index < right_size){
            comp++; // comparison took place !
            result.push_back(right[r_index]);
            r_index++;
        }
    }
    else{
        while(l_index < left_size){
            comp++; // comparison took place !
            result.push_back(left[l_index]);
            l_index ++;
        }
    }

    return result;
}

void NumberList::print_list(){
    cout << "Sorted List: "<<endl;
    for (int i = 0; i < data.size(); i++ ){
        cout << data[i]<<endl;
    }

    cout << endl<<"Number of comparisons: "<< comp<<endl;
    cout << "Number of swaps: " << swaps<<endl<<endl;
}

void NumberList::sort(const int choice){

    if (choice == 1) {
       data = merge_sort(data);
    }
    else
        selection_sort();

}
