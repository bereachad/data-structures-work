#include <fstream>
#include<vector>

using namespace std;

class NumberList{

    public:
        //constructor
        NumberList();

        //will perform a search, passing in a parameter so we can
        // use the class vcariables without using accessor functions
        void sort(const int);

        //merge sort algorithm.
        // the parameter is for the end of the data we are working on
        // since this is a recursive function.
        vector<int> merge_sort(const vector<int>&);

        //selection sort algorithm
        void selection_sort();

        vector<int> merge(const vector<int>&left, const vector<int>&right);

        void print_list();

    private:
        vector<int> data;
        ifstream inFile;
        int comp;
        int swaps;
};
