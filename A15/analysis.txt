Chad Peruggia
Assignment 15


1) The merge sort algorithm works by splitting the problem up into smaller sets.  It takes the original data, splits in in half, over and over, untill there are just two elements left, than will compare them.  It than moves up a level (Recursive Function) and compares the two returns to merge them in the correct order.  I know if the first element of the right is smaller than the first element of the left, the sides need to switch, else I can just put on the right at the end of the left.  They are similar because they are still comparing values multiple times, the difference is, Merge sort most of the time, ,does not need to compare as often, nor does it need to swap all the time.  It will only swap if required in that half.  Selection sort swaps very often, one element at a time.

2) The number of comparisons and swaps in an isolated condition match the expected measurments.  As we get to very large samples, it is clear how much the Merge Sort out performs the selection sort.

3) With Smaller sequences, the selection sort performs just as well as the Merge sort.  When I get to the large Sample set, the selection sort takes much longer to complete than the merge sort application.  I see a pattern in which the merge sort time growth is small, compared to the time for the selection sort which seems exponential.
