/**********************************************************
 * File: BinaryNumber.cc
 **********************************************************/
#include "adder.h"

BinaryNumber::BinaryNumber() {
  leastSignificantBit = NULL;
}

Bit*
BinaryNumber::get_least_significant_bit() const {
  return leastSignificantBit;
}

void
BinaryNumber::convert_decimal_to_binary( const int decimal ) {
  if( decimal < 0 ) return;

  /* Now use the algorithm to convert a decimal number to binary. It
     would repeatedly divide the number by 2 and keep the
     remainder. It will build the binary number this way, and due to
     the way that a linked list is created,  */

  // Start the process with the first binary number.
  leastSignificantBit = new Bit;
  if( decimal%2 == 1 ) leastSignificantBit->set_bit();
  else leastSignificantBit->clear_bit();

  // Now loop through the decimal and convert to binary.
  Bit* pntr = leastSignificantBit;
  int remainder = 0;
  int quotient = decimal/2;

  while( quotient > 0 ) {
    remainder = quotient % 2;
    pntr->add_next_bit( remainder == 1 );
    pntr = pntr->get_next_bit(); // Advance the pointer
    quotient = quotient / 2;
  }

}

void
BinaryNumber::remove_all() {

  if( leastSignificantBit == NULL ) return;
  Bit* trailer = leastSignificantBit;
  Bit* forward = trailer->get_next_bit();

  delete trailer;

  while( forward != NULL ) {
    trailer = forward;
    forward = forward->get_next_bit();
    delete trailer;
  }
}



#include <string>
ostream&
operator << ( ostream& outs, BinaryNumber& theNumber ) {
  Bit* bitPntr = theNumber.leastSignificantBit;
  string output = "";
  while( bitPntr != NULL ) {
    if( bitPntr->get_bit() ) output = "1"+output;
    else output = "0"+output;
    bitPntr = bitPntr->get_next_bit();
  }
  outs << output;
  return outs;
}

BinaryNumber operator + (const BinaryNumber& A, const BinaryNumber& B){

    BinaryNumber C;

    int carry =0;
    int sum(0);
    //these two variables are used to create the new linked list that will be returned and make sure we know where the beinging is.
    C.leastSignificantBit = new Bit;
    Bit* pntr =  C.leastSignificantBit;
    // we need to have pointers for A and B list so we can traverse them!
    Bit* aPntr = A.leastSignificantBit;
    Bit* bPntr = B.leastSignificantBit;

    // we start by creating the first new link in teh sum !
    if (aPntr->get_bit()) // if A is true
            sum = sum + 1;
    if (bPntr ->get_bit())// if B is true
            sum =sum + 1;
    //cout <<"HERE HERE HERE !!! @:!"<<endl;

    if( sum == 1 ){
        C.leastSignificantBit->set_bit(); // set to true, carry stays 0
    }

    else if (sum == 2 ) {  // we set to false and carry to true
        C.leastSignificantBit->clear_bit();
         carry = 1;
     }

    // move to the next links in each list
    aPntr = aPntr->get_next_bit();
    bPntr = bPntr->get_next_bit();

    while (aPntr != NULL || bPntr!=NULL){
        sum = 0; // rest sum for each step.;
        // if statments to see what is true in teh bits stored.
        if (aPntr->get_bit()) // if A is true
            sum = sum + 1;
        if (bPntr ->get_bit())// if B is true
            sum =sum + 1;

        sum = sum + carry;
        if (sum  == 1 ) // we set bit to true, and carry to 0;
        {
            carry = 0;
            pntr->add_next_bit (1);
            pntr = pntr ->get_next_bit();
        }
        else if (sum == 2) // we set bit to false and carry to true;
        {
            carry = 1;
            pntr->add_next_bit (0);
            pntr = pntr ->get_next_bit();
        }
        else if (sum == 3 ) // we set bit to true and carry to true
        {
            carry = 1;
            pntr->add_next_bit (1);
            pntr = pntr ->get_next_bit();
        }
        else if (sum == 0) // we set both to false;
        {
            carry = 0;
            pntr->add_next_bit (0);
            pntr = pntr ->get_next_bit();
        }

        // we need to build in functionality here to PAD the stuff if it is shorter.
        // we can check to see if A is at null and B is not, create a Bit at A , and vice
        // versa

            if (aPntr->get_next_bit() == NULL && bPntr->get_next_bit() !=NULL)  // a is to short
                aPntr->add_next_bit(0);
            else if (aPntr->get_next_bit() !=NULL && bPntr->get_next_bit() == NULL) // b is to short
                bPntr ->add_next_bit(0);


        aPntr = aPntr->get_next_bit();
        bPntr = bPntr->get_next_bit();

    }

    // we have to see if there is a carry left over, if so we make a new bit and put it in the list!
    if (carry == 1){
        pntr ->add_next_bit(1);
        pntr->get_next_bit();
    }

   return C;
}


BinaryNumber& BinaryNumber:: operator = (const BinaryNumber& rightObject){
    bool temp;

    if (this != &rightObject) // avoid self assignment;
    {

        this -> remove_all();
        this->leastSignificantBit = new Bit;
    //leastSignificantBit = this->leastSignificantBit;
    temp = rightObject.leastSignificantBit->get_bit(); // variable to hold the actual value of the least signifigant bit of the RHS.

    if (temp == 1){
        leastSignificantBit ->set_bit();
    }
    else //(temp == false)
        leastSignificantBit ->clear_bit();


    Bit* b_pntr = rightObject.get_least_significant_bit();  // pointer for return object.
    Bit* r_pntr = leastSignificantBit;
    b_pntr = b_pntr-> get_next_bit(); // Advance the pointer // needed because we already did the LSB.

    while (b_pntr != NULL){
        temp = b_pntr->get_bit();
        r_pntr->add_next_bit(b_pntr->get_bit());
        r_pntr = r_pntr->get_next_bit();
        b_pntr = b_pntr-> get_next_bit(); // Advance the pointer

    }
}

    //return object assigned
    return *this;
}

BinaryNumber BinaryNumber:: add(const BinaryNumber& object2){

    *this = *this + object2;
    return *this;
}