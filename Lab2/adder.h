/**********************************************************
 * File: BinaryNumber.h
 **********************************************************/

#include <ostream>
#include <iostream>
// the file Bit.h already includes <cstdlib> and uses namespace std
#include "bit.h"

class BinaryNumber {
 public:
  BinaryNumber();

  // ACCESSOR FUNCTION
  Bit* get_least_significant_bit() const;
  // precondition: The decimal input >= 0
  // postcondition: leastSignificantBit will point to the first link
  // of a linked list representing the "decimal" number in reverse
  // order (least to most significant bit
  void convert_decimal_to_binary( const int decimal );

  // OVERLOADED OUTPUT STREAM OPERATOR
  friend ostream& operator << ( ostream& outs, BinaryNumber& myBN );



  // precondition: none
  // postcondition: All the links in the linked list started by
  // leastSignificantBit will be de-allocated.
  void remove_all();

//overloaded friend operator to add two of the lists.
//precondition: A and B have been made into link lists.
//postcondition: will return the linked list for a return to C.
friend BinaryNumber operator + (const BinaryNumber& A,const BinaryNumber& B);


//overloaded assignment operator!!
BinaryNumber& operator = (const BinaryNumber& rightObject);

BinaryNumber add(const BinaryNumber& B);

 private:
  Bit* leastSignificantBit;
};
