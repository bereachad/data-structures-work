#include <cstdlib>
using namespace std;

class Bit {
 public:
  Bit() { nextBit = NULL; bitValue = false; }

  Bit* get_next_bit() const { return nextBit; }

  bool get_bit() const { return bitValue; }

  void set_bit() { bitValue = true; }
  void clear_bit() { bitValue = false; }

  void add_next_bit( bool initValue ) {
    nextBit = new Bit;
    if( initValue == true ) nextBit->set_bit();
  }

 private:
  bool bitValue;
  Bit* nextBit;
};
