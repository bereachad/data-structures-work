#include <iostream>
#include "adder.h"

using namespace std;

int main(){

    BinaryNumber A, B, C; // creates the linked list objects for each number to be represented in binary
    int in_a(-1), in_b(-1);
    cout <<"***************************************************"<<endl;
    cout <<"*        Welcome to the Binary number Adder!      *"<<endl;
    cout <<"***************************************************"<<endl<<endl<<endl<<endl;

    cout<<"Enter the first number in Decimal format (whole Number): ";
    cin >> in_a;
    while (in_a < 0 ){
        cout<<"Invalid number! Must be non negative"<<endl<<endl;
        cout<<"Enter Number: ";
        cin>>in_a;
    }


    cout<<"Enter the second number in Decimal format (whole Number): ";
    cin >> in_b;
    while (in_b < 0 ){
        cout<<"Invalid number! Must be non negative"<<endl<<endl;
        cout<<"Enter Number: ";
        cin>>in_b;
    }

    A.convert_decimal_to_binary(in_a);
    B.convert_decimal_to_binary(in_b);
    cout <<"A: " <<A<<endl;
    cout <<"B: "<<B<<endl;

    cout <<"Adding A + B = C" <<endl;
    C= A + B;
    cout <<"C is equal to : " << C<<endl<<endl;

    cout <<"Now performing  A = A + B"<<endl;
    A.add(B);
    cout <<"A is now equal to: "<< A << endl<<endl;

    cout<<"Now adding the new A to B.  C = A + B"<<endl;
    C= A + B;
    cout <<"C is now equal to: " << C<<endl<<endl;


    cout <<"Now displaying the overloaded = operator! (A = B)"<<endl;
    A = B;
    cout <<"A is : " << A <<endl;
}