#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

using namespace std;

struct treeNode{
    string data;
    treeNode *left, *right;
};



class tree{

    public:
        tree();
        //precondition: File told us how many expressions there were, we still have expressions to evaluate.
        //postcondition: We will populate a new binary tree, and display it in post notation.
        void newExpression();

        //precondition: There are tree's to be populated
        //postcondition: Will populate the tree with data from the inFile.
        void populate_tree(treeNode* &current);

        //precondition: Tree has been populated
        //postcondition: Return a string with the post notation expression.
        string pst_not(treeNode* current);


    private:
        string expressionData; // string for teh line of data to be parsed.
        int num_expressions;
        treeNode* ROOT; // the root of the entire tree;
        treeNode* current; // where we currenlty are in the tree;
        stringstream sstream;
        ifstream inFile;

};
