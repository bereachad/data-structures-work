#include "tree.h"

tree::tree(){ // default constructor
    char junk;
    // open the filestream
    inFile.open("expressions.in");
    //read in the num_expressions;
    inFile >> num_expressions;
    inFile.get(junk); // get rid of teh newline on the first line

    // get the expressions line by line, into a string stream, and than populate the tree's
    for (int i = 0; i < num_expressions; i++){
        cout <<"Expression: " << i+1<<endl;
        newExpression();
    }


}

void tree::newExpression(){

    getline(inFile, expressionData); // set the expression data for this new expression
    sstream.clear();
    sstream.str(expressionData);
    cout << "POPULATING TREE!"<< endl;
    //cout <<"EXPRESSION DATA!" << expressionData << endl;
    populate_tree(ROOT);

    cout << "POST NOTATION: ";
    cout << pst_not(ROOT);
    cout <<endl<<endl<<endl;
}

void tree::populate_tree(treeNode* &current){
    string temp; // temp variable used to hold where we are in the stringstream.

    sstream >> temp;
    // check to see what temp is
    if (temp == "*" || temp=="+" || temp == "/" || temp== "-")
    {
        //cout <<"here is an operator "<<temp<<endl;
        current= new treeNode;
        if ( ROOT == NULL )
        {
            ROOT = current;
        }
        current->data = temp;
        populate_tree(current->left); // we call the function on the left pointer, since we know we go left side first.
        populate_tree(current->right);// when done, we start to go down the right hand side .
    }
    else // its an integer
    {
        current = new treeNode;
        current->data = temp;
        current ->left = NULL; // set left pointer to null since we are at a number
        current ->right = NULL; // set right pointer to null since we are at a number.
    }
}


string tree::pst_not(treeNode *current){
    string rstring = "";
    string temp;

    // If the left tree contains something, get it.
    if (current->left){
        rstring.append(pst_not(current->left));
        rstring.append(" ");
    }

    //if right tree has something, go down and get it.
    if(current->right){
        rstring.append(pst_not(current->right));
        rstring.append(" ");
    }
    temp = current->data;
    // append the data on the stirng
    rstring.append(current->data);
    return rstring;
}
