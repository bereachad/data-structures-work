/***************************************************
 * File: stringstreamtest.cc
 * A small test to use a stringstream object to convert an input that
 * was a string object into a true integer type.
 ***************************************************/
#include <sstream>
#include <string>
#include <iostream>
using namespace std;

int main() {
  // get the input from the user as a string.
  string test;
  cin >> test; 

  // put this string into a "stream" similar to a file stream or cin
  stringstream stream(test);

  int integer;
  // Voila! Use the stream as you would cin, and the conversion is done!
  stream >> integer;

  integer+=1000;  // just to double check, att a thousand and display it.
  cout << integer << endl;
}
