#include <fstream>
#include <iostream>

using namespace std;

class compress{
    public:
        //constructor
        compress();
        // print function
        void print();

        //precondition: file has been read in, object created;
        //postcondition: sets the member variable to the user input
        void set_threshold(const int threshold);

        //postcondition : returns the size of the picture read in from file.
        int get_size();

        void do_work(int start_x, int end_x, int start_y, int end_y);

    private:
        ifstream inFile;
        int image_size;
        char ** picture;
        int threshold;


};
