/*CSC 236 Assignment 11
Chad Peruggia  & Torey Crayton
This assignment was the problem H from the ACM programming contest last year.  Our method was to use a data-structure of a 2D dynamic array made to the size
of the input file's first number.  We would than call a recursive function, which would call itself 4 times (once for each quadrant) If the majority
was not within the threshold values, it would do this untill there was no more levels to go down, if this happened we would just have the orignal values of
the image.
*/




#include "compress.h"
#include <iostream>

using namespace std;

int main(){
    int input_thresh;
    int size;
    compress image;


    cout <<"What is the desired threshold? " ;
    cin >> input_thresh;

    image.set_threshold(input_thresh);

    size = image.get_size();
    image.do_work(0, size, 0, size);
    cout << "Image: "<< input_thresh << "%"<<endl;
    image.print();

}
