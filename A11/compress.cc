#include "compress.h"
#include <iostream>

using namespace std;


compress::compress(){
    //intial step : open the input file.
    inFile.open("compress.in");
    char temp;

    if (inFile.fail()) // open of the file failed!
    {
        cerr <<" compress.in does not exsist.  Exiting program!"<<endl;
    }

    //read in the size of the image.
    inFile >> image_size;
    temp = inFile.get(); // this line is here to get rid of the \n at the end of the image_size line;

    //create dymanic array for the image.
    picture = new char *[image_size];

    for (int i = 0 ; i < image_size; i ++ )
        picture[i] = new char [image_size];

    // now read in the stuff from the file.
    for (int i = 0; i <image_size; i ++ ){
        for(int p=0; p<image_size; p ++){
            picture[i][p] = inFile.get();
        }
        temp = inFile.get(); // get rid of the junk new line
    }
}


void compress::print(){
    for (int i = 0; i <image_size; i ++ ){
        for(int p=0; p<image_size; p ++){
            cout << picture[i][p];
        }
    cout <<endl;
    }

}

void compress::set_threshold(const int input){
    threshold = input;
}

void compress::do_work(int start_x, int end_x, int start_y, int end_y){

 // first count current region
  int ones = 0;
  for (int x=start_x; x < end_x; x++)
    for (int y=start_y; y< end_y; y++)
      if (picture[y][x] == '1') // if the index in the array is a 1.
        ones++;

  int total = (end_x-start_x)*(end_y-start_y); // the total amount of values in this quadrant, for example a 2 * 2, meaning 4 values.

  int whats_majority = max(ones, total-ones);  // finds max between the ones and the "non-ones" or amount of 0's.


  if (whats_majority*100  >= threshold * total) {  // basecase.
      // we know that if a majority is either 1's or 0 and it is higher than the threshold required we can change
      // all the numbers in this case to fit them.
    char replace_val;

    // this bit of code is to check if there is more ones or 0's based on the threshold.
    if (ones > total-ones)
        replace_val ='1';
    else
        replace_val= '0';

    // this loop will set all numbers in that area to be the same if it is required.
    for (int x=start_x; x < end_x; x++)
      for (int y=start_y; y< end_y; y++)
        picture[y][x] = replace_val;
  }
  else { // we need to call the function on each quadrant of the quadrants!!
    // four quadrants in proper order
    int middle_x = (start_x+ end_x) / 2; //half way across
    int middle_y = (start_y + end_y) / 2; //half-way down.

    do_work(middle_x, end_x, start_y, middle_y);    // top-right
    do_work(start_x, middle_x,start_y, middle_y);  // top-left
    do_work(start_x, middle_x, middle_y,end_y);    // bottom-left
    do_work(middle_x, end_x, middle_y, end_y);      // bottom-right
  }

}

int compress::get_size(){
    return image_size;
}
