#include <iostream>
#include <fstream>

#include <string>

#include <deque>
#include <queue>
#include <list>

using namespace std;

struct table{
    int size;
    int seated;
    string name;
};

class Rest{
    public:

        int total_tables;
        Rest() {}; // contructor

        //postcondition: File has been read in and tables have been created. WIll palce them in the empty_A queue.
        void user_file_input();



        //precondition: User has selected a person is coming, the resturant has been read in correclty.
        //postcondition: A table will be moved from empty to full, and the corresponding data will be changed.
        void seat_person();

        //precondition: User has selected someone is leaving
        //postcondition: The table is emptied and moved to the empty table list.
        void person_leaving();

        //precondition: Resturant table list has been read in
        //postcondition: Floor plan is shown, including empty / full tables.
        void print_floor_plan();

        //preconditon: Person was placed in line
        //postcondition: Prints the people in teh line
        void print_line();

        //precondition: Person just left the resturant.
        //postcondition: Will seat someone from a line if they can fit.
        void seat_person_line();

        //precondtion: Person selected they want to quit.
        //post condition: Will quit if they are sure.
        bool quit();


        //precondition: user is sure they want to quit.
        //post condition: all queues are cleared out.
        void delete_data_structures();

        //DEBUGGING FUNCTIONS




    private:
        //int total_tables;
        //list of various queues we will be using.
        queue<table>empty_a;
        queue<table>empty_b;
        queue<table>line_a;
        queue<table>line_b;
        queue<table>full_a;
        queue<table>full_b;

        ifstream inFile;



};
