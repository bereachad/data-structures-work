#include "rest.h"



void Rest::user_file_input(){
    string filename;
    int s;
    table temp; // temp structure used while reading in the tables.

    cout <<"Please enter the File with the Resturant Table Arrangement:";
    cin >> filename;
    inFile.open(filename.c_str());
    inFile >> total_tables;

   while (inFile >> s)  {
        temp.size = s;
        temp.name="";
        temp.seated=0;
        empty_a.push(temp);
    }

}

void Rest:: seat_person(){
    int p_size;
    string p_name;
    table incoming; // create a table of the party size.
    table floor_plan;
    bool seated=false; // bool to see if we need to check empty_b. We need ot know if they were seated in an empty table from empty_a list.
    cout <<"What is the party size?";
    cin >> p_size;
    cout << "What is the party's name?";
    cin >> p_name;

    incoming.seated = p_size;
    incoming.name = p_name;

    // we have to find if there is a table that can fit them;
    while (!empty_a.empty() && seated == false){
        floor_plan = empty_a.front();
        if (incoming.seated == floor_plan.size || (incoming.seated) == floor_plan.size-1) // we have found a table where tehy can fit!
        {
            floor_plan.seated = incoming.seated;
            floor_plan.name = incoming.name;
            seated = true;
            empty_a.pop(); // pop it from empty
            full_a.push(floor_plan); // place it the full list.
        }
        else // the table is not hte right size, move it the empty_b list.
        {
            empty_a.pop();
            empty_b.push(floor_plan);
        }
    }

    // we need to check empty_ b now, if this is empty with no table found, we know the person needs to just stand in line.
    while(!empty_b.empty()  && seated == false){
        floor_plan = empty_b.front();
        if ((incoming.seated == floor_plan.size || (incoming.seated) == floor_plan.size-1)) // we have found a table where tehy can fit!
        {
            floor_plan.seated = incoming.seated;
            floor_plan.name = incoming.name;
            seated = true;
            empty_b.pop(); // pop it from empty
            full_a.push(floor_plan); // place it the full list.
            break;
        }
        else // the table is not hte right size, move it the line list, this is due to them having to wait now!, move it back to empty_a
        {
            empty_a.push(floor_plan); // place the table back into empty_a
            empty_b.pop(); // remove that table from empty_b
        }
    }

    if (seated == true)
        print_floor_plan();
    else{
        line_a.push(incoming); // push the incoming group onto a line!
        print_line();
    }
}

void Rest:: person_leaving(){
    //declare variables for user input
    int l_size;
    string l_name;
    table temp;
    bool found= false;

    cout <<endl<<"What is the parties name? ";
    cin >> l_name;
    cout << "What is the party size? ";
    cin >> l_size;

    // now we search the full_tables to find one that matches this information!
    while (!full_a.empty() && found == false){
        temp = full_a.front();
    // check size first , if not correct, we just move onto the next one.
        if ( temp.seated == l_size)// party size matches a table.
        {
            //check the name now!
            if ( temp.name == l_name){
                // WE HAVE A MATCH!! Clear out the data, and move that table to the empty list.
                found = true;
                temp.name="";
                temp.seated=0;
                empty_a.push(temp);
                full_a.pop(); // remove it from the full tables.
            }
            //else do nothing.
        }
        else //do nothing, we need to go to the next full table to do the check. push the full one into full_b , and pop it from A
        {
            full_b.push(temp);
            full_a.pop();
        }
    }

    // check the other list of full tables.
    while (!full_b.empty() && found == false){
        temp = full_b.front();
    // check size first , if not correct, we just move onto the next one.
        if ( temp.seated == l_size)// party size matches a table.
        {
            //check the name now!
            if ( temp.name == l_name){
                // WE HAVE A MATCH!! Clear out the data, and move that table to the empty list.
                found = true;
                temp.name="";
                temp.seated=0;
                empty_a.push(temp);
                full_b.pop(); // remove it from the full tables.
            }
            //else do nothing.
        }
        else{ // move the table back to full_a and pop off full_b
            full_a.push(temp);
            full_b.pop();
        }
    }

    if (found == false) // table doesn't exsist.
        cout <<" This party isn't here!" ;
    else
    {
        print_floor_plan();
        seat_person_line();
    }

}

void Rest:: print_floor_plan(){
    queue<table> temp; // temp que used for printing purpose
    table printing;
    cout <<"****************** TABLES ******************"<<endl;
    cout <<"** Capacity ***** Seated ***** Party Name **"<<endl;

    //print empty tables in A
    temp = empty_a;
    while (!temp.empty()){
    printing = temp.front();
    cout << "**    "<<printing.size << "     ***     " <<printing.seated <<"      ***   "<<printing.name<<endl;
    temp.pop();
    }

    //print empty tables in B
    temp = empty_b;
    while (!temp.empty()){
    printing = temp.front();
    cout << "**    "<<printing.size << "     ***     " <<printing.seated <<"      ***   "<<printing.name<<endl;
    temp.pop();
    }
    //preint full tables A
    temp= full_a;
    while (!temp.empty()){
    printing = temp.front();
    cout << "**    "<<printing.size << "     ***     " <<printing.seated <<"      ***   "<<printing.name<<endl;
    temp.pop();
    }

    //print full tables b
    temp = full_b;
    while (!temp.empty()){
    printing = temp.front();
    cout << "**    "<<printing.size << "     ***     " <<printing.seated <<"      ***   "<<printing.name<<endl;
    temp.pop();
    }
}

void Rest:: print_line(){
    cout <<endl<<endl;
    queue<table> temp; // temp queue to hold the line so we can print without popping of the original.
    table printing;

    cout <<"Customer placed in line.  Here is who is in line."<<endl<<endl;

    temp = line_a;
    while (!temp.empty()){
    printing = temp.front();
    cout << "Name: "<< printing.name<<"\tParty Size: "<< printing.seated<<endl;
    temp.pop();
    }

    temp = line_b;
    while (!temp.empty()){
    printing = temp.front();
    cout << "Name: "<< printing.name<<" Party Size: "<< printing.seated<<endl;
    temp.pop();
    }
    cout <<endl<<endl;

}


void Rest::seat_person_line(){

    table incoming; // create a table of the party size.
    table floor_plan;
    bool seated=false; // bool to see if we need to check empty_b. We need ot know if they were seated in an empty table from empty_a list.
    cout <<"Person left, seating from lines and updating floor plan!"<<endl<<endl;

while (!line_a.empty()) // while there are still people in line_a
{
    incoming = line_a.front();

    // we have to find if there is a table that can fit them;
    while (!empty_a.empty() && seated == false){
        floor_plan = empty_a.front();
        if (incoming.seated == floor_plan.size || (incoming.seated) == floor_plan.size-1) // we have found a table where tehy can fit!
        {
            floor_plan.seated = incoming.seated;
            floor_plan.name = incoming.name;
            seated = true;
            empty_a.pop(); // pop it from empty
            full_a.push(floor_plan); // place it the full list.
        }
        else // the table is not hte right size, move it the empty_b list.
        {
            empty_a.pop();
            empty_b.push(floor_plan);
        }
    }

    // we need to check empty_ b now, if this is empty with no table found, we know the person needs to just stand in line.
    while(!empty_b.empty()  && seated == false){
        floor_plan = empty_b.front();
        if ((incoming.seated == floor_plan.size || (incoming.seated) == floor_plan.size-1)) // we have found a table where tehy can fit!
        {
            floor_plan.seated = incoming.seated;
            floor_plan.name = incoming.name;
            seated = true;
            empty_b.pop(); // pop it from empty
            full_a.push(floor_plan); // place it the full list.
            break;
        }
        else // the table is not hte right size, move it the line list, this is due to them having to wait now!, move it back to empty_a
        {
            empty_a.push(floor_plan); // place the table back into empty_a
            empty_b.pop(); // remove that table from empty_b
        }
    }

    // we now move those people into line b, and clear them from line A as we continue to check line A
        line_b.push(incoming);
        line_a.pop();
}

// we know all people placed in line are placed in A, since no table to fit them was found, we can move the people from B back into
//line A, since B was used as a placeholder.
    while (!line_b.empty()) // we use a loop here to make sure we free the space here instead of just doing a copy.
    {
        incoming = line_b.front();
        line_a.push(incoming);
        line_b.pop();
    }

    if (seated == true)
        print_floor_plan();
    else{
        cout <<"Line is still the same."<<endl<<endl;
        print_line();
    }

}

bool Rest:: quit(){
    char sure;
    if (!line_a.empty() || !line_b.empty()) // one of the lines has people in it!
    {
        cout <<endl<<"Are you SURE you want to exit? There are people in line.(Y/N)"<<endl;
        cin >> sure;
        if (sure == 'Y' || sure == 'y'){
            return true;
            delete_data_structures();
            cout <<endl<<endl<<"Thank you for using the Resturant Table Manager";
        }
        else{
            cout <<endl<<endl<<"Thank you for using the Resturant Table Manager";
            return false;
        }
    }
    else // there was no line, just quit.
        return true;
}

void Rest::delete_data_structures(){
    //empty lines
    while(!line_a.empty()){
        line_a.pop();
        }
    while(!line_b.empty()){
        line_a.pop();
        }
    //empty full
    while(!full_a.empty()){
        full_a.pop();
        }
    while(!full_b.empty()){
        full_b.pop();
        }
    //empty emptytables.
    while(!empty_a.empty()){
        empty_a.pop();
        }
    while(!empty_b.empty()){
        empty_b.pop();
        }

}
