/*************************************************************
 * File: LinkedList.h
 * CSC236 A06 - Manipulating linked lists

 * This class has been implemented for you except for two member
 * functions that you must complete. The first, insert_link(...)
 * inserts a Link with a value that is the argument into the list, and
 * the second, remove_link(...)  removes a Link with a specific value
 * from the list. For the remove_link(...) function, if no Links have
 * that value in the list, nothing happens of course.
 *************************************************************/
#include <cstdlib>
#include <ostream>

using namespace std;

struct Link {
  int data;
  Link* nextLink;
};

class LinkedList {
 public:
  // Default constructor has nothing in it.
  LinkedList() { firstLink = NULL; }

  // Alternate constructor creates a link with input value right away.
  // This constructor will not be used for this program, but it is to
  // illustrate the necessity to "bootstrap" the insertion
  // process. The "insert" function will need to have something like
  // the code in here for the FIRST value inserted into the list.

  LinkedList( int value ) {
    firstLink = new Link;
    firstLink->data = value;
    firstLink->nextLink = NULL;
  }

  // The functions have been implemented for you.

  // precondition: none.
  // postcondition: if the input value is inside the list, it will
  // return true and false otherwise.
  // Notice that the function is defined as "const", which means that
  // the contents of the list should not change
  bool exists( int value ) const;

  // precondition: none
  // postcondition: the list is output, one element at a time into the
  // out stream, in which each value is separated by commas.
  friend ostream& operator << ( ostream& outs, LinkedList& theList );

  // This function should insert a Link with the input value at the
  // front of the list. In other words, if the list has the values:
  //      34 -> 11 -> 100 -> 94
  // and insert_link( 28 ) is called, the list would look like:
  //      28 -> 34 -> 11 -> 100 -> 94
  // WARNING: Be careful to handle the situation where there is
  // nothing in the list, i.e. firstLink is NULL
  void insert_link( int value );

  // This function should remove the first occurrence of a Link with
  // the input value, even if there are multiple Links with that
  // value. If the list has the values:
  //      34 -> 11 -> 100 -> 94 -> 100
  // and remove_link( 100 ) is called, the list would look like:
  //      34 -> 11 -> 94 -> 100
  // WARNING: Be careful to handle the situation where the input
  // value is in the first Link (i.e. firstLink->data == value )!
  void remove_link( int value );

 private:
  Link* firstLink;
  Link* newlink;
};
