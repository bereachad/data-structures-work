/*************************************************************
 * File: linked_dr.cc
 * CSC236 A06 - Manipulating linked lists

 * The main() function asks the user to populate the list be
 * repeatedly asking for a number and inserting a Link with that value
 * to the front of the list using the insert_link(...) function until
 * the user types in a negative number. It then asks the user for a
 * number to search and outputs whether or not it is there by calling
 * the exists(...) member function. Finally, after displaying the list
 * once, it asks the user for a number and calls the remove_link(...)
 * function to remove the first occurrence of a Link with that number
 * from the list and then displays the list again.
 *************************************************************/

#include "LinkedList.h"
#include <iostream>

using namespace std;

int
main() {

  LinkedList myList;
  
  cout << "Welcome! Please type in positive numbers to input into the \n"
       << "Linked List, using a negative number to stop.\n";

  int input;
  cout << "First number? ";
  cin >> input;

  while( input > 0 ) {
    myList.insert_link( input );
    cout << "Next number? ";
    cin >> input;
  }

  cout << "Number to search? ";
  cin >> input;
  if( myList.exists( input ) ) cout << input << " FOUND!";
  else cout << input << " not there.";
  cout << "\n";

  cout << "List " << myList << endl;

  cout << "Number to delete? ";
  cin >> input;
  myList.remove_link( input );
  cout << "List " << myList << endl;
}
