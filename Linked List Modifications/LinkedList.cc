/*************************************************************
 * File: LinkedList.cc
 * CSC236 A06 - Manipulating linked lists
 *************************************************************/
#include "LinkedList.h"

bool
LinkedList::exists( int value ) const {
  Link* currentLink = firstLink;

  // while we have not gone beyond the end of the linked list
  while( currentLink != NULL ) {

    // we found the value in the struct referenced by currentLink
    if( currentLink->data == value ) return true;

    // Advance the pointer to next item
    currentLink = currentLink->nextLink;
  }

  // we only get here if the if statement inside the while loop was
  // never true (i.e. the value is not in the linked list
  return false;
}

//the following include is so that we can use the "cout" function
#include <iostream>
using namespace std;

void
LinkedList::insert_link( int value ) {
    if (firstLink == NULL ) // the first link hasn't been created! Make it now
    {
        firstLink = new Link;
        firstLink->data = value;
        firstLink->nextLink= NULL;
    }
   else { // there is a link, put this new Link before it!
        newlink = new Link;  // create a new link
        newlink ->data = value;  // fills the data in the new link
        newlink ->nextLink = firstLink;  // points the new link to the old first link
        firstLink = newlink; // sets the first link to the link we just made
    }
}

void
LinkedList::remove_link( int value ) {
    bool found = false ; // bool used for loop, will turn to true when data is found
    Link* temp;
    Link* currentLink = firstLink;

  if (firstLink == NULL){ // there is nothing to delete
    cerr << "Can't delete from an empty list!"<<endl;
  }
  else{
      if (firstLink-> data == value) // first link to be deleted
      {
          temp = firstLink;
          firstLink = firstLink->nextLink;
         // delete temp occurs when function is over
            if (firstLink == NULL ) // there is only one node!
                delete currentLink;
      }
      else // search the list for that node with info!
      {
          temp = firstLink; // pointer to first node;
          currentLink= firstLink-> nextLink;
          /* This loop will go untill the end of the list or the object to be deleted is found.  Once it is found it moves to deleting  */
          while (currentLink != NULL && !found){

            if(currentLink->data != value){
                temp = currentLink;
                currentLink = currentLink ->nextLink;
            }
            else
                found = true;
          } // end loop to find where the value is.

          if (found) // time to delete!!!
          {
              temp->nextLink = currentLink->nextLink;
              delete currentLink;
          }
          else // the item isn't even in the list
            cout <<"The item isn't even in the list!"<<endl;

      }
  }

}

ostream&
operator << ( ostream& outs, LinkedList& theList ) {

  // Make a pointer to a Link that will be the one travelling down the
  // list. Note that this statement is the only time we refer to
  // theList.
  Link* traveller = theList.firstLink;

  // While we have not fallen off the list, output it and put a comma.
  while( traveller != NULL ) {
    outs << traveller->data ;
    // Output a comma as long as it is not the last thing on the list.
    if( traveller->nextLink != NULL ) cout << ", ";
    traveller = traveller->nextLink;
  }
  return outs;
}
