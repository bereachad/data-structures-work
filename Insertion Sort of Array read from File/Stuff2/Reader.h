/*******************************************************************
 * File: Reader.h
 * To be used in assignment number 3.

 * This program asks the user to type in the names of the files to
 * open to read and write an array. You are implement a sorting
 * algorithm as a member function for this class.

*******************************************************************/

#include <string>
#include <fstream>
using namespace std;
class Reader {
 public:
  static const int SIZE = 50;

  // Constructors
  Reader();

  // manipulator functions: I/O operations
  // ---------- open_IO() ---------------
  // precondition: File name has been entered by the user.
  // postcondition: Will return true if file can be opened, false if it can't
  bool open_IO();

  // ---------- close_IO() ---------------
  // postcondition: Will close the file upon completion of the function
  void close_IO();

  // ---------- read_array() -------------
  // precondition: File has been opened successfully
  // function operation: Will read in the characters / Numbers located in the file
  bool read_array();

  // ---------- write_array() ------------
  // precondition: Array has been sorted after being read.
  // postcondition: Will create a new file and print them to this array.
  bool write_array();

  // ---------- sort() ------------
  // precondition: The array has values read in it, and readValues >
  // 0. The sorting algorithm is whatever makes sense, but there must
  // be comments about how it works.
  void sort();

 private:
  // ---------- ask_filenames() ------------
  // preconditions: The filenames have not been asked before. It will
  // overwrite any old files with the new ones
  // postconditions: The user will have input the filenames, and the
  // flag askedFileNames will be set to true.
  void ask_filenames();

  int array[SIZE];
  int readSize;

  // input file names and stream to get the data
  string readFileName;
  ifstream inFile;

  // output file names and stream to get the data
  string writeFileName;
  ofstream outFile;
  bool askedFileNames;
};
