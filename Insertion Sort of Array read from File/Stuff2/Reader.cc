// File: Reader.cc

#include "Reader.h"
#include <iostream>
using namespace std;

/***************************************************************************
 * Contructors and modifying functions defined in "Reader.h"
***************************************************************************/
Reader::Reader() {
  askedFileNames = false;
  readFileName = "";
  writeFileName = "";
  for( int i = 0; i<SIZE; i++ ) array[i]=0;
}

bool Reader::open_IO(){

    do{
    inFile.clear();
    readFileName="";
    ask_filenames();
    inFile.open(readFileName.c_str());
    outFile.open(writeFileName.c_str());
    }
    while(inFile.fail());

    return true;
}




void Reader::close_IO() {
    inFile.close();
}

bool Reader::read_array() {
    int read(0); // variable to see how many numbers are read so far
    inFile>>readSize;

    do{
        inFile>>array[read];
        read++;
    }while(!inFile.eof() && read!=readSize && read <50);

    readSize=read;

    sort(); // call sort function;

    return true; // return true for function bool value.

}


bool Reader::write_array() {

    for (int i = 0; i < readSize; i ++){
        outFile << array[i]<<endl;
    }

    return true;
}

void Reader::sort() { //insertion sort method
    int temp ;// variables used in swaps

    for (int index =1; index < readSize; index++)
    {
        int newSpot=index; // start from 2nd spot in array and move up
        while( newSpot > 0 && array[newSpot-1] > array[newSpot] ) { // if the spot before is greater then current, swtich them
                temp = array[newSpot-1];
                array[newSpot-1] = array[newSpot];
                array[newSpot] = temp;
              newSpot --;	// decrement newSpot to move the insertion downwards
        }
    }
}

/*--------------------------------------------------------------
  This function asks the user for the filenames. This operation
  is placed in a separate function because it is called multiple
  times.
  --------------------------------------------------------------*/
void Reader::ask_filenames() {
  cout << "Welcome. Please type in the name of the file to read the numbers.\n";
  cin >> readFileName;
  cout << "Thank you. Please type in the name of the file to write the numbers.\n";
  cin >> writeFileName;
  askedFileNames = true;
}
