#include "Reader.h"

#include <iostream>

int main() {
  Reader myReader;

  if( myReader.open_IO() ) {
    myReader.read_array();
    myReader.write_array();
    myReader.close_IO();
  }
}
