#include <vector>
#include <fstream>
#include <iostream>

using namespace std;

const int MAX_SIZE = 512;


struct node{
    int data;
    node* left, *right,*parent;
};

class tree{

    public:
        //constructor
        tree();

        //precondtion: in file exsists in directory;
        //postcondition: will create the binary tree from the input file.
        void create_tree();


        void node_create(vector<int>&data, int low, int high, node*& current);


        void printTree(node* current);

    private:
        int num_elements;
        ifstream inFile;
        int sum; // rolling sum of all teh numbers as we make the nodes.
        int odd; // rolling sum of all the odd numbers we come across as we make tree;
        int leaves; // num of leaves in the tree.
        int height ; // height
        int internal;
        int max_dif; // max dif between root and non root.

        node* Root; // root node;
        node* currentNode; // current node while populating the tree.

};
