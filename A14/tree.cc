#include "tree.h"
#include<cmath>
#include <cstdlib>
using namespace std;


tree::tree(){
    num_elements = 0;
    leaves = 0;
    sum = 0;
    internal=0;
    max_dif =0;
    odd = 0;
    height = 0 ; // height starts at 1, since the root is level 1.
    inFile.open("numbers.in");
    Root = NULL;
}

void tree::create_tree(){
    vector<int> temp; // data structure to temp hold the data when we read it in.
    int data;
    int mid,  front;


    while (inFile >> data)
    {

        temp.push_back(data);
    }

    // find out how many elements there are so we know where the 1/2 way mark is.
    num_elements = temp.size();
    front = 0;

  //  cout << num_elements << endl << mid;
   // cout << temp[num_elements-1];

   // create the root node first.

    node_create(temp, front, num_elements-1, Root);
    cout << "Number of internal Nodes: "<< internal+1 << endl; // we have to add one because of the root node;
    cout << "Number of Leaves: " <<leaves<< endl;
    cout << "Height of the tree "<< height<< endl;
    cout << "Odd Numbers in the Tree:"<<odd<<endl;
    cout << "Sum of Numbers in the Tree: " <<sum<<endl;
    cout << "Max Difference found : " <<max_dif<< endl;
}

void tree::node_create(vector<int>&data, int low, int high, node*& current){

    //Find the middle element of the subtree and put that at the top of the tree.
    //Find all the elements before the middle element and use this algorithm recursively to get the left subtree.
    //Find all the elements after the middle element and use this algorithm recursively to get the right subtree.
    int dif(0);
    int mid = (low+high) /2 ;
    current = new node;


    current->left = current->right =NULL;
    current->data = NULL;


    /* If there are values between low and high
    that are lower than mid, add them to the left.
    If there are values between low and high that
    are higher than mid, add them to the right.*/


    if (low < mid){
        internal++;
        height++;
        node_create(data, low,mid-1,current->left);
    }

    current->data = data[mid];

    /* The calculations for odd, and sum*/
    sum += data[mid];

    if ( data[mid] % 2 != 0) // we have an odd number, lets add it in.
        odd++;

    if (mid < high){
        internal++;
        node_create(data, mid+1, high, current->right);
    }

    // calculate teh number of leaves
    if (mid == high ){
        leaves++;
        internal --; // subtract from internal because it was a leave
    }

     // calculate the max diff
    if (mid-1 > 0) {
        dif = abs ( data[mid] - data[mid-1]);
        if ( dif > max_dif)
            max_dif = dif;
    }

}
