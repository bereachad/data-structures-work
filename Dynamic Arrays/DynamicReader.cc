#include "DynamicReader.h"
#include <iostream>
#include <cstdlib>

using namespace std;
DynamicReader::DynamicReader() {

  dimensions = 1;
  oneDArray = NULL;
  readSize = 0;

  twoDArray = NULL;
  nRows = 0;
  nCols = 0;

  readFileName = "";
  writeFileName = "";

  askedFileNames = false;
}

// REMEMBER TO CLEAN UP/DEALLOCATE THE ARRAYS!
DynamicReader::~DynamicReader() {
    delete oneDArray;

    for (int i=0; i<nRows; i++ ) {
        delete twoDArray[i];
    }


}

// THIS FUNCTION IS SOMETHING YOU HAVE IMPLEMENTED ALREADY
bool DynamicReader::open_IO(){


    do{
    inFile.clear();
    readFileName="";
    ask_filenames();
    inFile.open(readFileName.c_str());
    outFile.open(writeFileName.c_str());
    }
    while(inFile.fail());

    return true;

}


// THIS FUNCTION IS SOMETHING YOU HAVE IMPLEMENTED ALREADY
void DynamicReader::close_IO() {
    inFile.close();
    outFile.close();
}

/*--------------------------------------------------------------

  This function asks the user for the filenames. This operation is
  placed in a separate function because it is called multiple
  times. Although it is awkward, ask the user for the dimensions of
  the data structure in this function as well.

  --------------------------------------------------------------*/
void DynamicReader::ask_filenames() {
  cout << "Welcome. Please type in the name of the file to read the numbers.\n";
  cin >> readFileName;
  cout << "Thank you. Please type in the name of the file to write the numbers.\n";
  cin >> writeFileName;
  askedFileNames = true;

  cout << "What are the dimensions of this data structure?\n";
  cin >> dimensions;
}

bool DynamicReader::read_1D_array() {


    inFile >> claimedSize;
    oneDArray = new int [claimedSize];

    do{

    if (readSize <= claimedSize){ // claimed has not been meet.
        inFile >> oneDArray[readSize];
        readSize++;
    }
    else //claimed size was hit, break out of loop!
        break;

    }while (!inFile.eof());


    //check to see if it broke out before end of file, if this was the case
    //there was still integers to be read, output error message.
    if (!inFile.eof() && readSize!= claimedSize ){
        cerr << "There were to many numbers!"<<endl;
        return true;
    }

    if (readSize-1 < claimedSize) // there weren't enough numbers
    {
        cerr << "There weren't enough numbers!";
        //pad the array with 0's

        for (int i = readSize-1; readSize < claimedSize; i ++ )
        {
            oneDArray[i]=0;
        }

        return false;
    }

    // if none of the above conditions are true, the end of file has been meet
    // and the claimed size is readsize, it is perfect.
    return true;


}


bool DynamicReader::read_2D_array() {
    // delete the 1D Array

    readSize=0;
    int cols(0);
    int rows(0);
	inFile >> nRows;
	inFile >>nCols;

    //lines below to make 2D array of size rows / cols
	twoDArray = new int* [nRows];
	for (int i = 0; i <nRows; i ++ ){
		twoDArray[i] = new int [nCols];
		}
	///////////////////////////////////////////////////////


	do{

	    if (cols == nCols ) {// that column has filled, time to go to the next row and reset column
	        //do a check to see if the rows are filled now too before we reset the values and increase row
	        // if the rows are filled, there are to many numbers on the file, we need to break out
	        // of this loop.
	        if (rows == nRows){
                break;
	        }
	        else{ // there is still space for the next row.
            rows ++; // next row to begin
            cols = 0 ;
	        }
	    }
	    else { // perform the normal steps
            inFile >> twoDArray [rows][cols];
            readSize ++;
            cols++; // next column space
	    }

	}while (!inFile.eof() && rows < nRows);

	//see if there were more then supposed.
	if (!inFile.eof() && rows !=nRows){ // not the end of file and the rows aren't the same
        cerr << "There were to many rows!!"<<endl;
        return true;
	}

	if (rows < nRows) // we know there werent enough numbers
	{
	    cerr << "There aren't enough numbers!"<<endl;
	    //pad the array with 0's
	    // since we don't want to display 0's where there was no data, we will
	    // do the row check using readSize. We know the readsize / columns will
	    // be the number of rows there must be.
	    /*for (int r = rows; r < readSize /cols; r++){
            for (int c = cols; c < nCols; c++)
                {
                    twoDArray[r][c]=0;
                }
	    }*/

	    return false;



	}

	return true;

}

/*--------------------------------------------------------------
  write_array() writes the array into the file with the name
  writeFileName.
  precondition: The output file is open, and nothing has been
  written to the file before.

  Remember that the output should have the same format as the
  input.
 --------------------------------------------------------------*/
// TAKE A LOOK AT THE read_array() FUNCTION IN THE HEADER FILE FOR HOW
// TO HANDLE THE DIFFERENCE BETWEEN 1D and 2D ARRAYS.
bool DynamicReader::write_array() {

    if (dimensions==1 ){ // single dimenstion array
        outFile << claimedSize<<endl;
        for (int i = 0; i < readSize -1 ; i++)
            if (oneDArray[i]!= 0) //we dont want to print the padded 0's
                outFile << oneDArray[i] <<endl;
    }

    if (dimensions==2){ // two dimension array
        outFile << nRows << " " << nCols <<endl;
        for (int r=0; r < nRows; r++){
            for (int c = 0 ; c < nCols ; c++){
                if (twoDArray[r][c]!=0) // we dont want to print padded 0's
                outFile <<twoDArray[r][c]<<" ";
            }
            outFile<<endl;
        }
    }

    return true;

}

ostream& operator << ( ostream& outs, DynamicReader& me ) {

    if (me.dimensions==1 ){ // single dimenstion array
        outs <<me.claimedSize<<endl;
        for (int i = 0; i < me.readSize -1 ; i++){
        if (me.oneDArray[i]!= 0) //we dont want to print the padded 0's
            outs << me.oneDArray[i] <<endl;
        }
    }

   if (me.dimensions==2){ // two dimension array
        outs << me.nRows << " " << me.nCols <<endl;
        for (int r=0; r < me.nRows; r++){
            for (int c = 0 ; c < me.nCols ; c++){
                if (me.twoDArray[r][c]!=0) // we dont want to print padded 0's
                outs <<me.twoDArray[r][c]<<" ";
            }
            outs<<endl;
        }
    }

  return outs;
}
