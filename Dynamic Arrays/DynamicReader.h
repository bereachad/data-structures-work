/*******************************************************************
 * File: DynamicReader.h
 * To be used in assignment number 3.
 * This program asks the user to type in the names of the files to
 * open to read and write a dynamically allocated array.

 You are to implement this program in two parts:

 1) Create a single dimensional array (a vector) to store the
    values. This array is stored in a file of the user's choice. The
    first number in this file indicates the number of elements in the
    file to be stored. You are to use this number to create the array
    space. The numbers themselves follow.

 2) Create a two dimensional array (a 2D array or matrix) and load the
    values from a file of the user's choosing. The first two numbers
    in the file indicate the size of the array (say, N and M), which
    you can create the matrix. The next N * M numbers are the numbers
    in the array in row-major form.

    For example, a 2D array of 4 rows and 5 columns of the numbers 1
    through 20 can look like this:

    4 5
    1 2 3 4 5
    6 7 8 9 10
    11 12 13 14 15
    16 17 18 19 20

  You are to implement four functions:

  1) read_1D_array() and read_2D_array() to create and read in arrays
     of one and two dimensions

  2) write_array(), which can write both two and two dimension arrays

     into the file with the name writeFileName, depending on what
     value dimensions has. The format of the output is EXACTLY the
     same as in the input.  In other words, a 2D array would first
     have the number of rows and the number of columns on the first
     line, and then all the contents of each row output per line.
     # CLARIFICATION ADDED SEPT 25,2010

     A caveat for the 2D array is that this function must output the
     original data from the input file. Therefore, if the input was:

     2 4
     1 2 3

     The array itself will be of size 2x4 with the first 3 numbers as
     1 2 3 and then the rest padded as zeros. However, you do not
     output:

     2 4
     1 2 3 0
     0 0 0 0

  3) the << operator, which is similar to the write_array() function
     except it is more general purpose.

*******************************************************************/

#include <string>
#include <fstream>
using namespace std;
class DynamicReader {
 public:

  // Constructors:
  // Make sure that both arrays are set to blank.
  DynamicReader();

  ~DynamicReader();

  // manipulator functions: I/O operations
  // ---------- open_IO() ---------------
  // precondition: The names of the files to open and close have
  // already been asked and stored in the respective member
  // variables. It is in fact flexible enough to get the filenames
  // from the user if necessary
  // postcondition: Returns true if the streams are open, and error
  // messages and false is returned otherwise. If the output file
  // opening fails, the input file is closed because the program
  // cannot do anything.
  // THIS FUNCTION HAS BEEN IMPLEMENTED FOR YOU
  bool open_IO();

  // ---------- close_IO() ---------------
  // postcondition: The files that were opened are closed.
  // THIS FUNCTION HAS BEEN IMPLEMENTED FOR YOU
  void close_IO();

  // THIS FUNCTION HAS BEEN IMPLEMENTED FOR YOU
  void read_array() {
    if( dimensions == 1 ) read_1D_array();
    if( dimensions == 2 ) read_2D_array();
  }

/*--------------------------------------------------------------
  write_array() writes the array into the file with the name
  writeFileName.
  precondition: The output file is open, and nothing has been
  written to the file before.

  For oneDArray, the dimension size is written followed by the numbers
  themselves. For twoDArray, the two dimension numbers are at the
  beginning of the file (number of rows, followed by number of
  columns), followed by the values.

  CLARIFICATION ADDED SEPT 25, 2010
  The output must be exactly the same as the input read in, without
  any padded zeros. Therefore, in some cases you cannot assume that
  the dimensions are in fact correct.

  --------------------------------------------------------------*/
  bool write_array();

  /*------------------------------------------------------------
    Overloaded << operator to handle outputing the DynamicReader
    class. It is important to return the stream once we are done in
    case we want to chain outputs together.
    ------------------------------------------------------------*/
  friend ostream& operator << ( ostream& out, DynamicReader& me );

 private:
/*--------------------------------------------------------------
  read_1Darray() and read_2Darray() both read in numbers from the file
  with the name readFileName. This function must first allocate space
  to hold the information and then read the elements from the file.

  precondition: The input file is open and the file pointer is at the
  start of the file (i.e. nothing has been read from the file yet). If
  you are reading into a single array (using oneDArray), the first
  number in the file is the "dimension number" and indicates how large
  an array to allocate.  For a matrix (to be stored in twoDArray), the
  first two numbers represent the number of rows and columns,
  respectively. After reading these numbers, the data structure of
  appropriate size is allocated before reading the data.

  Two possible errors are:

  1) the actual number of elements in the file is smaller than
     indicated by the dimension number(s). The program will output an
     error message, set readSize to how many were actually read, and
     then return false.
     CLARIFICATION ADDED SEPT 25, 2010
     The rest of the unfilled array is padded with zeros.

  2) the number of elements in the file is greater than the first
     number in the file.  In this case, put out a message stating that
     there are more integers but that only the dimension number(s) has
     been read.
     CLARIFICATION ADDED SEPT 25, 2010
     readSize will of course be the number of elements this program
     read.
  --------------------------------------------------------------*/
  bool read_1D_array();
  bool read_2D_array();

/*--------------------------------------------------------------
  preconditions: The filenames have not been asked before. It will
  overwrite any old files with the new ones
  postconditions: The user will have input the filenames, and the
  flag askedFileNames will be set to true.
  --------------------------------------------------------------*/
  // THIS FUNCTION HAS BEEN IMPLEMENTED FOR YOU
  void ask_filenames();

  int dimensions;  // The variable to store the number of dimensions
		   // we are recording. I.E. dimensions=1 for a single
		   // vector and dimension=2 for matrices
  // Variable for a single dimension array
  int* oneDArray;
  int readSize;
  int claimedSize;

  // Variables for a 2D array.
  int** twoDArray;
  int nRows, nCols;

  /*---------------------------------------------------------
    File I/O member variables
    ---------------------------------------------------------*/
  // input file names and stream to get the data
  string readFileName;
  ifstream inFile;

  // output file names and stream to get the data
  string writeFileName;
  ofstream outFile;
  bool askedFileNames;
};
