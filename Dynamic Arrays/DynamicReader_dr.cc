#include "DynamicReader.h"

#include <iostream>
using namespace std;
int main() {
  DynamicReader myReader;

  if( myReader.open_IO() ) {
    myReader.read_array();
    cout << myReader << endl;
    myReader.write_array();
    myReader.close_IO();
  }
}
